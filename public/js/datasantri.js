$(document).ready(function() {
  $('input[name="data_santri"]').on("keyup change", function() {
    var val = $(this).val();
    if (val) {
      $.ajax({
        url: app_url + "/admin/getSantriTsn",
        type: "GET",
        dataType: "json",
        data: { val: val },
        success: function(data) {
          $("#optionSantri").addClass("show");
          $("#optionSantri").empty();
          $.each(data, function(key, value) {
            $("#optionSantri").append(
              '<a href="' +
                app_url +
                "/admin/pendaftaran/add/" +
                value.id +
                '" class="dropdown-item" data-id="' +
                value.id +
                '">' +
                value.nama_lengkap +
                "</a>"
            );
          });
        }
      });
    } else {
      $("#optionSantri").empty();
    }
  });
});

// $(document).ready(function() {
//   $("#optionSantri .dropdown-item").on("click", function() {
//     var id = $(this).data("id");
//     if (id) {
//       $.ajax({
//         url: app_url + "/admin/getSantriTsnById",
//         type: "GET",
//         dataType: "json",
//         data: { val: val },
//         success: function(data) {
//           $.each(data, function(key, value) {
//             $("input[name='nama_lengkap']").val(value.nama_lengkap);
//           });
//         }
//       });
//     }
//   });
// });
// function checkPhoneNumber() {
//   $("#phoneNumber").on("keyup change", function() {
//     var phone = $("#phoneNumber").val();
//     if (phone) {
//       $.ajax({
//         url: app_url + "/daftar/check-phone-number",
//         type: "GET",
//         dataType: "json",
//         data: { phone: phone },
//         error: function(req, textStatus, errorThrown) {
//           //this is going to happen when you send something different from a 200 OK HTTP
//           checkPhoneNumber();
//         },
//         success: function(data) {
//           if (data.status == 1) {
//             $("#phone-validation").empty();
//             $("#phone-validation").append(
//               '<div class="alert alert-warning">No HP sudah terdaftar</strong></div>'
//             );
//             // $('#btnD').attr('disabled', 'disabled');
//             $("#alertInput").show();
//           } else {
//             $("#phone-validation").empty();
//             // $('#btnD').removeAttr('disabled');
//             $("#alertInput").hide();
//           }
//         }
//       });
//     }
//   });
// }
