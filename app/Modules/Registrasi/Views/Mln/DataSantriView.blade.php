{{-- @extends('Layouts::layout')
@section('content') --}}
    <br>
    <h5>Data Calon Santri</h5>
    <hr>
    <div class="row">
        <form action="{{
                !$dataCalonSantri ? url('/registrasi/mln/create') : 
                url('/registrasi/mln/update')
              }}"  
              method="POST" 
              role="form" 
              enctype="multipart/form-data" 
              class="col-md-8">
            {{ csrf_field() }}
            @if($dataCalonSantri)
            <input type="hidden" name="id" value="{{$dataCalonSantri->id}}"/>
            @endif 
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Nama Lengkap</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            onkeyup="this.value = this.value.toUpperCase()"
                                            name="nama_lengkap" 
                                            class="form-control form-control-sm" 
                                            placeholder="Nama lengkap" 
                                            required
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nama_lengkap}}"
                                            />
                                        {{-- <span>Wajib menggunakan hurup kapital, contoh: MUHAMMAD AHSAN</span> --}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">NISN</label>
                                    <div class="col-md-9">
                                                <input 
                                                    type="text" 
                                                    name="nisn" 
                                                    class="form-control form-control-sm" 
                                                    placeholder="NISN" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->nisn}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Hobi</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="hobi" 
                                            class="form-control form-control-sm" 
                                            placeholder="Hobi" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->hobi}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Cita-cita</label>
                                    <div class="col-md-9">
                                        <input 
                                            type="text" 
                                            name="cita_cita" 
                                            class="form-control form-control-sm" 
                                            placeholder="Cita-cita" 
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->cita_cita}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Tempat & Tanggal Lahir</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-sm" 
                                                    name="tempat_lahir" 
                                                    placeholder="Tempat lahir" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->tempat_lahir}}">
                                            </div>
                                            @if($dataCalonSantri)
                                            @php
                                                $tanggal = explode('-', $dataCalonSantri->tanggal_lahir);
                                                $hari = $tanggal[2];
                                                $bulan = $tanggal[1];
                                                $tahun = $tanggal[0];
                                            @endphp
                                            @else
                                            @php
                                                $hari = null;
                                                $bulan = null;
                                                $tahun = null;
                                            @endphp
                                            @endif
                                            <div class="col-md-3">
                                                <select class="form-control form-control-sm" name="hari" required>
                                                    <option value="{{!$hari ? '' : $hari}}">
                                                        {{!$hari ? 'Tanggal' : $hari}}
                                                    </option>
                                                    @for($i=1; $i<=31; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control form-control-sm" name="bulan" required>
                                                    <option value="{{!$bulan ? '' : $bulan}}">
                                                        {{!$bulan ? 'Bulan' : $bulan}}
                                                    </option>
                                                    <option value='1'>Januari</option>
                                                    <option value='2'>Februari</option>
                                                    <option value='3'>Maret</option>
                                                    <option value='4'>April</option>
                                                    <option value='5'>Mei</option>
                                                    <option value='6'>Juni</option>
                                                    <option value='7'>Juli</option>
                                                    <option value='8'>Agustus</option>
                                                    <option value='9'>September</option>
                                                    <option value='10'>Oktober</option>
                                                    <option value='11'>November</option>
                                                    <option value='12'>Desember</option>
                                                    {{-- @for($i=1; $i<=12; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor --}}
                                                </select>
                                            </div>
                                            <div class="col-md-3">
                                                <select class="form-control form-control-sm" name="tahun" required>
                                                    <option value="{{!$tahun ? '' : $tahun}}">
                                                        {{!$tahun ? 'Tahun' : $tahun}}
                                                    </option>
                                                    @for($i=2001; $i<=2010; $i++)
                                                        <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Jenis Kelamin</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="jenis_kelamin" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->jenis_kelamin}}">
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->jenis_kelamin}}
                                            </option>
                                            <option value="Laki-laki">Laki-laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Anak ke</label>
                                    <div class="col-md-2">
                                        <input 
                                            type="number" 
                                            class="form-control form-control-sm" 
                                            name="anak_ke" 
                                            placeholder="" 
                                            required
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->anak_ke}}">
                                    </div>
                                    <label class="col-md-2 col-form-label col-form-label-sm">dari</label>
                                    <div class="col-md-2">
                                        <input 
                                            type="number" 
                                            class="form-control form-control-sm" 
                                            name="dari_bersaudara"
                                            placeholder="" 
                                            required
                                            value="{{!$dataCalonSantri ? '' : $dataCalonSantri->dari_bersaudara}}">
                                    </div>
                                    <label class="col-md-2 col-form-label col-form-label-sm">bersaudara</label>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Nama Sekolah Asal</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-sm" 
                                                    name="sekolah_asal"
                                                    placeholder="Nama Sekolah Asal" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->sekolah_asal}}">
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control form-control-sm" name="status_sekolah">
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->status_sekolah}}">
                                                            {{!$dataCalonSantri ? 'Status Sekolah' : $dataCalonSantri->status_sekolah}}
                                                    </option>
                                                    <option value="Negeri">Negeri</option>
                                                    <option value="Swasta">Swasta</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Alamat Sekolah Asal</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <input 
                                                    type="text" 
                                                    class="form-control form-control-sm" 
                                                    name="alamat_sekolah_asal"
                                                    placeholder="Alamat Sekolah Asal" 
                                                    required
                                                    value="{{!$dataCalonSantri ? '' : $dataCalonSantri->alamat_sekolah_asal}}">
                                            </div>
                                            <div class="col-md-4">
                                                <select name="tahun_lulus" class="form-control form-control-sm">
                                                    <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->tahun_lulus}}">
                                                        {{!$dataCalonSantri ? 'Tahun Lulus' : $dataCalonSantri->tahun_lulus}}
                                                    </option>
                                                    <option value="2019">2019</option>
                                                    <option value="2018">2018</option>
                                                    <option value="2017">2017</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Kategori Sekolah Asal</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="kategori_sekolah_asal" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->kategori_sekolah_asal}}">
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->kategori_sekolah_asal}}
                                            </option>
                                            <option value="MTS Persis Tarogong">MTs Persis Tarogong</option>
                                            <option value="MTs Lain">MTs Lain</option>
                                            <option value="SMP Islam">SMP Islam</option>
                                            <option value="SMP Umum/Negeri">SMP Umum/Negeri</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Rencana tinggal saat belajar</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="rencana_tinggal" required>
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->rencana_tinggal}}">
                                                           {{!$dataCalonSantri ? '' : $dataCalonSantri->rencana_tinggal}}
                                            </option>
                                            <option value="Asrama">Asrama</option>
                                            <option value="Rumah Orang Tua">Rumah Orang Tua</option>
                                            <option value="Rumah Saudara / Wali">Rumah Saudara / Wali</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Dorongan masuk pesantren</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="dorongan_masuk" >
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->dorongan_masuk}}">
                                                           {{!$dataCalonSantri ? '' : $dataCalonSantri->dorongan_masuk}}
                                            </option>
                                            <option value="Diri Sendiri">Diri Sendiri</option>
                                            <option value="Orang Tua">Orang Tua</option>
                                            <option value="Saudara">Saudara</option>
                                            <option value="Teman">Teman</option>
                                            <option value="Lainnya">Lainnya</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 col-form-label col-form-label-sm">Sumber informasi PSB</label>
                                    <div class="col-md-9">
                                        <select class="form-control form-control-sm" name="sumber_informasi" id="" >
                                            <option value="{{!$dataCalonSantri ? '' : $dataCalonSantri->sumber_informasi}}">
                                                    {{!$dataCalonSantri ? '' : $dataCalonSantri->sumber_informasi}}
                                            </option>
                                            <option value="Keluarga">Keluarga</option>
                                            <option value="Teman">Teman</option>
                                            <option value="Brosur">Brosur</option>
                                            <option value="Majalah">Majalah</option>
                                            <option value="Internet">Website</option>
                                            <option value="Sekolah">Sekolah</option>
                                            <option value="Medsos">Medsos</option>
                                            <option value="Spanduk">Spanduk</option>
                                            <option value="Lainnya">Lainnya</option>

                                        </select>
                                    </div>
                                </div>







            <br>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
{{-- @endsection --}}