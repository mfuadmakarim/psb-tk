@extends('Layouts::layout')
@section('content')
<div class="container" style="padding-top: 15px; padding-bottom: 100px;">
	<div class="col-md-6 offset-md-3">
		<div class="row" style="margin-bottom: 10px">
			<div class="col-md-12 text-center">
					<img width="100" src="{{asset('public/images/persistarogonglogo.png')}}">
					<h3>Pendaftaran Santri Baru TK Persis Tarogong</h3>
			</div>
		</div>
		<div class="row">
			<div class="alert alert-danger text-center" style="width:100%">
				<!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
				<h3>Maaf, Pendaftaran Gagal.</h4>
				<h4>Jumlah pendaftar telah mencapai kuota.</h4>

			</div>
            <div class="col-md-12 text-center"><a href="{{url('/')}}" title="Kembali"><button type="submit" class="btn btn-danger" >Kembali</button></a></div>
		</div>
	</div>
</div>
@endsection