<?php

namespace App\Modules\Admin\Controllers;
use Validator;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
use App\Modules\Registrasi\Models\StatusRuanganModel;
use App\Modules\Registrasi\Models\StatusRuangRegModel;
use App\Modules\Admin\Models\DataSantriTsnModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminMlnController extends Controller
{
    //Data
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $user = Auth::user()->getAttributes();
            $role = $user['role'];
            if($role == 'admin_mln'){
                return $next($request);
            }else{
                return redirect('/login');
            }
        });
    }

    public function index(){
        $dataPendaftar = PendaftarMlnModel::orderBy('id', 'desc')->get();
        $dataPendaftarKosong = (new PendaftarMlnModel)->getPendaftarKosong();
       
        $dataCalonSantri = null;
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "";
        $separator = "/";
    	return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataPendaftarKosong' => $dataPendaftarKosong,
            'dataPendaftarDate' => $dataPendaftarDate,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title,
            'separator' => $separator
        ));
    }

    public function show($id){
        $dataCalonSantri = PendaftarMlnModel::find($id);
        
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "";
        $separator = "-";
    	return view('Admin::Mln.DetailSantriView', array(
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title,
            'separator' => $separator
        ));
    }

    public function dashboard(){
        $pendaftarMln = new PendaftarMlnModel;
        $dataPendaftar = $pendaftarMln
        ->where('no_tes', '!=', NULL)
        ->get();
        $total = count($dataPendaftar);
        $dataPendaftarJk = $pendaftarMln->getCountDataSantri('jenis_kelamin');
        $dataPendaftarJalurMasuk = $pendaftarMln->getCountDataSantri('jalur_masuk');
        $dataPendaftarSekolahAsal = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        $dataPendaftarRencanaTinggal = $pendaftarMln->getCountDataSantri('rencana_tinggal');
        $dataPendaftarPilihanJurusan1 = $pendaftarMln->getCountDataSantri('pilihan_jurusan_1');
        $dataPendaftarPilihanJurusan2 = $pendaftarMln->getCountDataSantri('pilihan_jurusan_2');
        $dataPendaftarPenghasilanOrangtua = $pendaftarMln->getCountDataSantri('Penghasilan_orangtua');
        $dataPendaftarKabupaten = $pendaftarMln->getCountDataSantri('Kabupaten');
        $dataPendaftarSumberInformasi = $pendaftarMln->getCountDataSantri('sumber_informasi');
        $dataPendaftarPetugasPendaftaran = $pendaftarMln->getCountDataSantri('petugas_pendaftaran');
        $dataPendaftarHarian = $pendaftarMln->getCountDataSantriByDate();
        // $dataPendaftarCaraPendaftaran = $pendaftarMln->getCountDataSantri('is_admin');
        // $dataPendaftarPilihanJurusan = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarPenghasilanOrtu = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarPenanggungBiaya = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarMotifMasuk = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarPekerjaanOrtu = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');
        // $dataPendaftarKotaAsal = $pendaftarMln->getCountDataSantri('kategori_sekolah_asal');

        return view('Admin::Mln.DashboardView', array(
            'dataPendaftarJk' => $dataPendaftarJk,
            'dataPendaftarJalurMasuk' =>  $dataPendaftarJalurMasuk,
            'dataPendaftarSekolahAsal' => $dataPendaftarSekolahAsal,
            'dataPendaftarRencanaTinggal' => $dataPendaftarRencanaTinggal,
            'dataPendaftarPilihanJurusan1' => $dataPendaftarPilihanJurusan1,
            'dataPendaftarPilihanJurusan2' => $dataPendaftarPilihanJurusan2,
            'dataPendaftarPenghasilanOrangtua' => $dataPendaftarPenghasilanOrangtua,
            'dataPendaftarKabupaten' =>  $dataPendaftarKabupaten,
            'dataPendaftarSumberInformasi' => $dataPendaftarSumberInformasi,
            'dataPendaftarPetugasPendaftaran' => $dataPendaftarPetugasPendaftaran,
            'dataPendaftarHarian' => $dataPendaftarHarian,
            'total' => $total,
        ));
    }

    public function indexPendaftarOnline(){
        $dataPendaftar = (new PendaftarMlnModel)->getPendaftarOnline();
        // dd($dataPendaftar);
        $dataCalonSantri = null;
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "Online";
        $separator = "/";
    	return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title,
            'separator' => $separator
        ));
    }

    public function indexPendaftarFinal(){
        $dataPendaftar = (new PendaftarMlnModel)->getPendaftarFinal();
        $dataCalonSantri = null;
        $provinces = $this->getProvinces();
        $type = "list";
        $title = "Final";
        $separator = "/";
    	return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title,
            'separator' => $separator
        ));
    }

    public function add($id){
        $dataPendaftar = PendaftarMlnModel::orderBy('id', 'desc')->get();
        $dataCalonSantri = DataSantriTsnModel::find($id);
        $provinces = $this->getProvinces();
        $type = "form";
        $title = "";
        $separator = "/";
        return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title,
            'separator' => $separator
        ));
    }

    public function edit($id){
        $dataPendaftar = PendaftarMlnModel::orderBy('id', 'desc')->get();
        $dataCalonSantri = PendaftarMlnModel::find($id);
        $provinces = $this->getProvinces();
        $type = "form";
        $title = "";
        $separator = "-";
        return view('Admin::Mln.ListSantriView', array(
            'dataPendaftar' => $dataPendaftar,
            'dataCalonSantri' => $dataCalonSantri,
            'provinces' => $provinces,
            'type' => $type,
            'title' => $title,
            'separator' => $separator,
            'state' => 'edit',
        ));
    }

    public function create(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required|unique:pendaftar_mln',
            'tempat_lahir' => 'required',
            'hari' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'jenis_kelamin' => 'required',
            'anak_ke' => 'required',
            'dari_bersaudara' => 'required',
            'sekolah_asal' => 'required',
            'alamat_sekolah_asal' => 'required',
            'kategori_sekolah_asal' => 'required',
            'rencana_tinggal' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'penghasilan_orangtua' => 'required',
            'penanggung_biaya' => 'required',
            'alamat_lengkap' => 'required',
            'provinceId' => 'required',
            'cityId' => 'required',
            'districtId' => 'required',
            'villageId' => 'required',
            'pilihan_jurusan_1' => 'required',
            'pilihan_jurusan_2' => 'required',
            'pilihan_pesantren' => 'required',
            'jalur_masuk' => 'required',
        ]);
        if ($validator->fails()) {
             return redirect('/admin')->withErrors($validator);
        }
        $pendaftarMln = new PendaftarMlnModel;
        
        $hari = $request->input('hari');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $tanggal_lahir = $tahun.'-'.$bulan.'-'.$hari;
        
        $pendaftarMln->is_admin = 1;
        $pendaftarMln->nama_lengkap = $request->input('nama_lengkap');
        $pendaftarMln->nisn = $request->input('nisn');
        $pendaftarMln->tahun_lulus = $request->input('tahun_lulus');
        $pendaftarMln->tempat_lahir = $request->input('tempat_lahir');
        $pendaftarMln->tanggal_lahir = $tanggal_lahir;
        $pendaftarMln->jenis_kelamin = $request->input('jenis_kelamin');
        $pendaftarMln->hobi = $request->input('hobi');
        $pendaftarMln->cita_cita = $request->input('cita_cita');
        $pendaftarMln->anak_ke = $request->input('anak_ke');
        $pendaftarMln->dari_bersaudara = $request->input('dari_bersaudara');
        $pendaftarMln->sekolah_asal = $request->input('sekolah_asal');
        $pendaftarMln->status_sekolah = $request->input('status_sekolah');
        $pendaftarMln->alamat_sekolah_asal = $request->input('alamat_sekolah_asal');
        $pendaftarMln->kategori_sekolah_asal = $request->input('kategori_sekolah_asal');
        $pendaftarMln->rencana_tinggal = $request->input('rencana_tinggal');
        $pendaftarMln->dorongan_masuk = $request->input('dorongan_masuk');
        $pendaftarMln->sumber_informasi = $request->input('sumber_informasi');
        $pendaftarMln->user_id = $userId;
        $pendaftarMln->nama_ayah = $request->input('nama_ayah');
        $pendaftarMln->pendidikan_ayah = $request->input('pendidikan_ayah');
        $pendaftarMln->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $pendaftarMln->nama_ibu = $request->input('nama_ibu');
        $pendaftarMln->pendidikan_ibu = $request->input('pendidikan_ibu');
        $pendaftarMln->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $pendaftarMln->nama_wali = $request->input('nama_wali');
        $pendaftarMln->pendidikan_wali = $request->input('pendidikan_wali');
        $pendaftarMln->pekerjaan_wali = $request->input('pekerjaan_wali');
        $pendaftarMln->hubungan_wali_anak = $request->input('hubungan_wali_anak');
        $pendaftarMln->penghasilan_orangtua = $request->input('penghasilan_orangtua');
        $pendaftarMln->penanggung_biaya = $request->input('penanggung_biaya');
        $pendaftarMln->alamat_lengkap = $request->input('alamat_lengkap');
        $pendaftarMln->provinsi = $request->input('provinceId');
        $pendaftarMln->kabupaten = $request->input('cityId');
        $pendaftarMln->kecamatan = $request->input('districtId');
        $pendaftarMln->kelurahan = $request->input('villageId');
        $pendaftarMln->no_hp_1 = $request->input('no_hp_1');
        $pendaftarMln->no_hp_2 = $request->input('no_hp_2');
        $pendaftarMln->pilihan_jurusan_1 = $request->input('pilihan_jurusan_1');
        $pendaftarMln->pilihan_jurusan_2 = $request->input('pilihan_jurusan_2');
        $pendaftarMln->pilihan_jurusan_3 = $request->input('pilihan_jurusan_3');
        $pendaftarMln->pilihan_pesantren = $request->input('pilihan_pesantren');
        $pendaftarMln->jalur_masuk = $request->input('jalur_masuk');
        $pendaftarMln->petugas_pendaftaran = $request->input('petugas_pendaftaran');
        $pendaftarMln->no_hp_wali = $request->input('no_hp_wali');
        $pendaftarMln->alamat_wali = $request->input('alamat_wali');
        $pendaftarMln->save();
        
        $id = $pendaftarMln->id;
        $this->updatePembayaran($id);
        return redirect('/admin')->with('status', 'Data berhasil disimpan');
    }

    public function update(Request $request){
        $userId = Auth::id();
        $validator = Validator::make($request->all(), [
            'nama_lengkap' => 'required',
            'tempat_lahir' => 'required',
            'hari' => 'required',
            'bulan' => 'required',
            'tahun' => 'required',
            'jenis_kelamin' => 'required',
            'anak_ke' => 'required',
            'dari_bersaudara' => 'required',
            'sekolah_asal' => 'required',
            'alamat_sekolah_asal' => 'required',
            'kategori_sekolah_asal' => 'required',
            'rencana_tinggal' => 'required',
            'nama_ayah' => 'required',
            'nama_ibu' => 'required',
            'penghasilan_orangtua' => 'required',
            'penanggung_biaya' => 'required',
            'alamat_lengkap' => 'required',
            'provinceId' => 'required',
            'cityId' => 'required',
            'districtId' => 'required',
            'villageId' => 'required',
            'pilihan_jurusan_1' => 'required',
            'pilihan_jurusan_2' => 'required',
            'pilihan_pesantren' => 'required',
            'jalur_masuk' => 'required',
        ]);
        if ($validator->fails()) {
             return redirect('/admin')->withErrors($validator);
        }
        $pendaftarMln = PendaftarMlnModel::find($request->input('id'));
        
        $hari = $request->input('hari');
        $bulan = $request->input('bulan');
        $tahun = $request->input('tahun');
        $tanggal_lahir = $tahun.'-'.$bulan.'-'.$hari;
        
        $pendaftarMln->is_admin = 1;
        $pendaftarMln->nama_lengkap = $request->input('nama_lengkap');
        $pendaftarMln->nisn = $request->input('nisn');
        $pendaftarMln->tahun_lulus = $request->input('tahun_lulus');
        $pendaftarMln->tempat_lahir = $request->input('tempat_lahir');
        $pendaftarMln->tanggal_lahir = $tanggal_lahir;
        $pendaftarMln->jenis_kelamin = $request->input('jenis_kelamin');
        $pendaftarMln->hobi = $request->input('hobi');
        $pendaftarMln->cita_cita = $request->input('cita_cita');
        $pendaftarMln->anak_ke = $request->input('anak_ke');
        $pendaftarMln->dari_bersaudara = $request->input('dari_bersaudara');
        $pendaftarMln->sekolah_asal = $request->input('sekolah_asal');
        $pendaftarMln->status_sekolah = $request->input('status_sekolah');
        $pendaftarMln->alamat_sekolah_asal = $request->input('alamat_sekolah_asal');
        $pendaftarMln->kategori_sekolah_asal = $request->input('kategori_sekolah_asal');
        $pendaftarMln->rencana_tinggal = $request->input('rencana_tinggal');
        $pendaftarMln->dorongan_masuk = $request->input('dorongan_masuk');
        $pendaftarMln->sumber_informasi = $request->input('sumber_informasi');
        $pendaftarMln->user_id = $userId;
        $pendaftarMln->nama_ayah = $request->input('nama_ayah');
        $pendaftarMln->pendidikan_ayah = $request->input('pendidikan_ayah');
        $pendaftarMln->pekerjaan_ayah = $request->input('pekerjaan_ayah');
        $pendaftarMln->nama_ibu = $request->input('nama_ibu');
        $pendaftarMln->pendidikan_ibu = $request->input('pendidikan_ibu');
        $pendaftarMln->pekerjaan_ibu = $request->input('pekerjaan_ibu');
        $pendaftarMln->nama_wali = $request->input('nama_wali');
        $pendaftarMln->pendidikan_wali = $request->input('pendidikan_wali');
        $pendaftarMln->pekerjaan_wali = $request->input('pekerjaan_wali');
        $pendaftarMln->hubungan_wali_anak = $request->input('hubungan_wali_anak');
        $pendaftarMln->penghasilan_orangtua = $request->input('penghasilan_orangtua');
        $pendaftarMln->penanggung_biaya = $request->input('penanggung_biaya');
        $pendaftarMln->alamat_lengkap = $request->input('alamat_lengkap');
        $pendaftarMln->provinsi = $request->input('provinceId');
        $pendaftarMln->kabupaten = $request->input('cityId');
        $pendaftarMln->kecamatan = $request->input('districtId');
        $pendaftarMln->kelurahan = $request->input('villageId');
        $pendaftarMln->no_hp_1 = $request->input('no_hp_1');
        $pendaftarMln->no_hp_2 = $request->input('no_hp_2');
        $pendaftarMln->pilihan_jurusan_1 = $request->input('pilihan_jurusan_1');
        $pendaftarMln->pilihan_jurusan_2 = $request->input('pilihan_jurusan_2');
        $pendaftarMln->pilihan_jurusan_3 = $request->input('pilihan_jurusan_3');
        $pendaftarMln->pilihan_pesantren = $request->input('pilihan_pesantren');
        $pendaftarMln->jalur_masuk = $request->input('jalur_masuk');
        $pendaftarMln->petugas_pendaftaran = $request->input('petugas_pendaftaran');
        $pendaftarMln->no_hp_wali = $request->input('no_hp_wali');
        $pendaftarMln->alamat_wali = $request->input('alamat_wali');
        $pendaftarMln->save();
        
        $id = $pendaftarMln->id;
        $this->updatePembayaranEdit($id);
        return redirect('/admin')->with('status', 'Data pendaftar berhasil disimpan');
    }

    public function indexPembayaran(){
        $dataPembayaran = PembayaranMlnModel::orderBy('id', 'desc')
        ->where('bank_penerima', NULL)
        ->get();
    	return view('Admin::Mln.ListSantriPembayaranView', array(
            'dataPembayaran' => $dataPembayaran
        ));
    }

    public function indexPembayaranOnline(){
        $dataPembayaran = PembayaranMlnModel::orderBy('id', 'desc')
        ->where('bank_penerima', 'BRI Syariah')
        ->get();
    	return view('Admin::Mln.ListSantriPembayaranOnlineView', array(
            'dataPembayaran' => $dataPembayaran
        ));
    }

    public function updatePembayaran($id){
        $userId = Auth::id();
        $pendaftarMln = PendaftarMlnModel::find($id);
        $pembayaranMln = new PembayaranMlnModel;
        
        $pembayaranMln->user_id = $pendaftarMln->user_id;
        $pembayaranMln->nama_lengkap = $pendaftarMln->nama_lengkap;
        $pembayaranMln->jumlah_transfer = "350000";
        $pembayaranMln->pendaftar_id = $id;
        $pembayaranMln->status = 'Menunggu Validasi';
        $pembayaranMln->no_tes = $pendaftarMln->no_tes;

        $pembayaranMln->save();
    }
    public function updatePembayaranEdit($id){
        $userId = Auth::id();
        $pendaftarMln = PendaftarMlnModel::find($id);
        $pembayaranMln = (new PembayaranMlnModel)->where('pendaftar_id', $id)->first();
        
        $pembayaranMln->user_id = $pendaftarMln->user_id;
        $pembayaranMln->nama_lengkap = $pendaftarMln->nama_lengkap;

        $pembayaranMln->save();
    }
    public function validasiPembayaran($id, $santriId){
        $userId = Auth::id();
        $dataPendaftar = PendaftarMlnModel::find($santriId);
        $pembayaranMln = PembayaranMlnModel::find($id);
        $statusRegistrasi = new StatusRegistrasiModel;
        $dataStatus = $statusRegistrasi->where('user_id', $userId)->first();

        $date = date("d-m-Y");

        $pembayaranMln->validasi_oleh = $userId;
        $pembayaranMln->status = "Dibayar";
        $pembayaranMln->tanggal_validasi = $date;
        $pembayaranMln->save();

        $statusRegistrasi->status_data_pembayaran = "2";

        if($pembayaranMln->status == "Dibayar"){
            $noTes = $this->generateNoTes($santriId);
            $dataPendaftar->no_tes = $noTes;
            $dataPendaftar->save();

            $noRuang = $this->generateRuangan($noTes);
            $dataPendaftar->no_ruang = $noRuang;
            $dataPendaftar->save();
        }
        return redirect('/admin/pembayaran')->with('status', 'Pembayaran divalidasi');

    }
    
    // fgsi lama
    // public function generateRuangan($noTes){    
    //     $statusRuangReg = (new StatusRuangRegModel)->where('no_tes', $noTes)->first();
    //     // dd($statusRuangReg);
    //     $ruang = $statusRuangReg->ruang;
    //     return $ruang;
    // }
    // public function generateNoTes($santriId){
    //     $pendaftarMln = new PendaftarMlnModel;
    //     $dataCalonSantri = $pendaftarMln->where('id', $santriId)->first();
    //     $jkCalonSantri = $dataCalonSantri->jenis_kelamin;
    //     $jalurMasuk = $dataCalonSantri->jalur_masuk;
    //     // dd($jalurMasuk.'<br>'.$jkCalonSantri);
    //     if($dataCalonSantri && 
    //       ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
    //       $jalurMasuk == 'Reguler'){
    //         $code = 'MA';
    //     }elseif($dataCalonSantri && 
    //       ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
    //       $jalurMasuk == 'Reguler'){
    //         $code = 'MB';
    //     }elseif($dataCalonSantri && 
    //       ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
    //       $jalurMasuk == 'Beasiswa'){
    //         $code = 'MC';
    //     }elseif($dataCalonSantri && 
    //       ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
    //       $jalurMasuk == 'Beasiswa'){
    //         $code = 'MD';
    //     }

    //     $statusRuangReg = new StatusRuangRegModel;
    //     $statusRuangReg->code = $code;
    //     $statusRuangReg->no_reg = $santriId;
    //     $statusRuangReg->save();

    //     $statusRuangId = $statusRuangReg->id;
    //     $statusRuang = $statusRuangReg->ruang;

    //     $count = $statusRuangReg->getCountCode($code);
    //     // dd($count);
    //     if($count % 20 != 0 && $count > 20){
    //         $ruangInt=substr($statusRuang,1);
    //         $statusRuang = str_pad(((int)$ruangInt)+1, 2, "0", STR_PAD_LEFT);
    //     }
    //     if($statusRuang == null){
    //         $statusRuang = "01";
    //     }
        
    //     $noTes = $code.$statusRuang.'-'.str_pad($count, 3, "0", STR_PAD_LEFT);
        
    //     if($dataCalonSantri && 
    //       ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
    //       $jalurMasuk == 'Reguler'){
    //         $codeRuang = 'A';
    //     }elseif($dataCalonSantri && 
    //       ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
    //       $jalurMasuk == 'Reguler'){
    //         $codeRuang = 'B';
    //     }elseif($dataCalonSantri && 
    //       ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
    //       $jalurMasuk == 'Beasiswa'){
    //         $codeRuang = 'C';
    //     }elseif($dataCalonSantri && 
    //       ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
    //       $jalurMasuk == 'Beasiswa'){
    //         $codeRuang = 'D';
    //     }
        
    //     $statusRuangReg->ruang = $codeRuang.$statusRuang;
    //     $statusRuangReg->no_tes = $noTes;
    //     $statusRuangReg->save();

    //     return $noTes;
    // }
    
    public function generateRuangan($noTes){    
        $statusRuangReg = (new StatusRuangRegModel)->where('no_tes', $noTes)->first();
        // dd($statusRuangReg);
        $ruang = $statusRuangReg->ruang;
        return $ruang;
    }
    public function generateNoTes($santriId){
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('id', $santriId)->first();
        $jkCalonSantri = $dataCalonSantri->jenis_kelamin;
        $jalurMasuk = $dataCalonSantri->jalur_masuk;
        // dd($jalurMasuk.'<br>'.$jkCalonSantri);
        if($dataCalonSantri && 
          ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
          $jalurMasuk == 'Reguler'){
            $code = 'MA';
        }elseif($dataCalonSantri && 
          ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
          $jalurMasuk == 'Reguler'){
            $code = 'MB';
        }elseif($dataCalonSantri && 
          ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
          $jalurMasuk == 'Beasiswa'){
            $code = 'MC';
        }elseif($dataCalonSantri && 
          ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
          $jalurMasuk == 'Beasiswa'){
            $code = 'MD';
        }

        $statusRuangReg = new StatusRuangRegModel;
        $statusRuangReg->code = $code;
        $statusRuangReg->no_reg = $santriId;
        $statusRuangReg->save();

        $statusRuangId = $statusRuangReg->id;
        $statusRuang = $statusRuangReg->ruang;

        // dd($statusRuang);

        $count = $statusRuangReg->getCountCode($code);

        $statusRuang = DB::table('status_ruang_reg')
                                ->where('code', $code)
                                ->orderBy('id', 'desc')
                                ->get();
        // dd($statusRuang);
        
        // if(($count-1) % 20 == 0){
        //     dd('code: ' .$code. ' | count:' .$count. ' | ' . $count % 20 . ' | ' .strval($statusRuang[2]+1));
        // }else{
        //     dd('code: ' .$code. ' | count:' .$count. ' | ' . $count % 20 . ' | ' .strval($statusRuang));
        // }
        // dd(substr($statusRuang));
        if($dataCalonSantri && 
          ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
          $jalurMasuk == 'Reguler'){
            $codeRuang = 'A';
        }elseif($dataCalonSantri && 
          ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
          $jalurMasuk == 'Reguler'){
            $codeRuang = 'B';
        }elseif($dataCalonSantri && 
          ($jkCalonSantri == 'Laki-laki' || $jkCalonSantri == 'L') &&
          $jalurMasuk == 'Beasiswa'){
            $codeRuang = 'C';
        }elseif($dataCalonSantri && 
          ($jkCalonSantri == 'Perempuan' || $jkCalonSantri == 'P') &&
          $jalurMasuk == 'Beasiswa'){
            $codeRuang = 'D';
        }

        if($count <= 1){
            $ruangPrev = $codeRuang.'01';
        }else if ($count > 1){
            $ruangPrev = $statusRuang[1]->ruang;
        }        

        if($count > 1 && ($count-1) % 20 == 0){
            $statusRuangNum = str_split($ruangPrev);
            // $statusRuang = str_pad(strval($statusRuangNum[2]+1), 2, "0", STR_PAD_LEFT);
            // dd($codeRuang.$statusRuang);
            $statusRuang = str_pad(floor($count/20)+1, 2, "0", STR_PAD_LEFT);
            $statusRuangReg->ruang = $codeRuang.$statusRuang;
            $noTes = $code.$statusRuang.'-'.str_pad($count, 3, "0", STR_PAD_LEFT);
            $statusRuangReg->no_tes = $noTes;
            $statusRuangReg->save();
            return $noTes;
            // dd($statusRuang);
        }
        if($statusRuang == null){
            $statusRuangReg->ruang = "01";
            $noTes = $code.'01-'.str_pad($count, 3, "0", STR_PAD_LEFT);
            $statusRuangReg->no_tes = $noTes;
            $statusRuangReg->save();
            return $noTes;
        }else{
            $statusRuangNum = str_split($ruangPrev);
            $statusRuangReg->ruang = $ruangPrev;
            // $statusRuang = str_split($ruangPrev);
            $statusRuang = str_pad(floor($count/20)+1, 2, "0", STR_PAD_LEFT);
            $noTes = $code.$statusRuang.'-'.str_pad($count, 3, "0", STR_PAD_LEFT);
            //$noTes = $code.$statusRuang[1].$statusRuang[2].'-'.str_pad($count, 3, "0", STR_PAD_LEFT);
            $statusRuangReg->no_tes = $noTes;
            $statusRuangReg->save();
            return $noTes;
        }
    }


    public function getProvinces(){
        return Facade::allProvinces();
    }
    public function getCities($provinceId){
        $data = Facade::findProvince($provinceId, 'cities');
        $cities = $data->cities;
        return $cities;
    }
    public function getDistricts($cityId){
        $data = Facade::findCity($cityId, 'districts');
        $districts = $data->districts;
        return $districts;
    }
    public function getVillages($districtId){
        $data = Facade::findDistrict($districtId, 'villages'); 
        $villages = $data->villages;
        return $villages;
    }

    public function getSantriTsn(Request $request){
        $keyword = $request->input("val");
        $dataSantri = new DataSantriTsnModel;
        $dataSearch = $dataSantri->where('nama_lengkap','LIKE','%'.$keyword.'%')->get();
        
        return $dataSearch;
    }

    public function getSantriTsnById(Request $request){
        $id = $request->input("id");
        $dataSantri = new DataSantriTsnModel;
        $data = $dataSantri->where('id', $id);

        return $data;
    }

}