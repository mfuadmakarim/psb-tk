{{-- @extends('Layouts::layout')
@section('content') --}}
    <br>
    <h5>Alamat</h5>
    <hr>
    <div class="row">
    	<form action="{{url('/registrasi/mln/create')}}" method="POST" role="form" enctype="multipart/form-data" class="col-md-8">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Alamat Lengkap</label>
                <div class="col-md-9">
                    <textarea 
                        type="text" 
                        name="nama_lengkap" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        required>
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Provinsi</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="tahun_lulus" class="form-control form-control-sm" required>
                                <option value="">Pendidikan</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Kabupaten / Kota</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="tahun_lulus" class="form-control form-control-sm" required>
                                <option value="">Pendidikan</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Kecamatan</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="tahun_lulus" class="form-control form-control-sm" required>
                                <option value="">Pendidikan</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Kelurahan / Desa</label>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-12">
                             <select name="tahun_lulus" class="form-control form-control-sm" required>
                                <option value="">Pendidikan</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>           
            <hr>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">No HP 1</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_lengkap" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">No HP 2</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_lengkap" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        required>
                </div>
            </div>
  
            <br>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>
{{-- @endsection --}}