<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHobiCitaToPendaftarMlnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendaftar_mln', function($table) {
            $table->string('hobi')->nullable();
            $table->string('cita_cita')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pendaftar_mln', function($table) {
            $table->dropColumn('hobi');
            $table->dropColumn('cita_cita');
        });
    }
}
