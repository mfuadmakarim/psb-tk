<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStepsToStatusRegistrasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_registrasi', function($table) {
            $table->integer('status_data_santri')->nullable();
            $table->integer('status_data_ortu')->nullable();
            $table->integer('status_data_alamat')->nullable();
            $table->integer('status_data_jurusan')->nullable();
            $table->integer('status_data_pembayaran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_registrasi', function($table) {
            $table->dropColumn('status_data_santri');
            $table->dropColumn('status_data_ortu');
            $table->dropColumn('status_data_alamat');
            $table->dropColumn('status_data_jurusan');
            $table->dropColumn('status_data_pembayaran');
        });
    }
}
