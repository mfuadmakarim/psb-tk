@extends('Layouts::adminLayout')
@section('content')
    <div class="container-fluid">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @if(isset($dataPendaftarHarian))
                        @foreach ($dataPendaftarHarian as $data)
                            <tr>
                                <td>{{$data->date}}</td>
                                <td>{{$data->total}}</td>
                            </tr>
                        @endforeach
                        {{-- <tr>
                            <td>Total</td>
                            <td>{{$total}}</td>
                        </tr> --}}
                    @endif
                </tr>
            </tbody>
        </table>
        
        <div class="card-columns mt-3">
           
                <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Jenis Kelamin</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarJk))
                                    @foreach ($dataPendaftarJk as $data)
                                        <tr>
                                            <td>{{$data->jenis_kelamin}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            
                <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Sekolah Asal</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Sekolah Asal</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarSekolahAsal))
                                    @foreach ($dataPendaftarSekolahAsal as $data)
                                        <tr>
                                            <td>{{$data->kategori_sekolah_asal}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Rencana Tinggal</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Rencana Tinggal</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarRencanaTinggal))
                                    @foreach ($dataPendaftarRencanaTinggal as $data)
                                        <tr>
                                            <td>{{$data->rencana_tinggal}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>


           
                <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Jalur Masuk</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Jalur Masuk</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarJalurMasuk))
                                    @foreach ($dataPendaftarJalurMasuk as $data)
                                        <tr>
                                            <td>{{$data->jalur_masuk}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Pilihan Jurusan 1</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Jurusan</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarPilihanJurusan1))
                                    @foreach ($dataPendaftarPilihanJurusan1 as $data)
                                        <tr>
                                            <td>{{$data->pilihan_jurusan_1}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                {{-- <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Pilihan Jurusan 2</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Jurusan</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarPilihanJurusan2))
                                    @foreach ($dataPendaftarPilihanJurusan2 as $data)
                                        <tr>
                                            <td>{{$data->pilihan_jurusan_2}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div> --}}
                
                <div class="card mb-3 bg-light">
                    <div class="card-header">Berdasarkan Penghasilan Orang Tua</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Penghasilan</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarPenghasilanOrangtua))
                                    @foreach ($dataPendaftarPenghasilanOrangtua as $data)
                                        <tr>
                                            <td>{{$data->Penghasilan_orangtua}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                {{-- <div class="card mb-3 bg-light">
                    <div class="card-header">Berdasarkan Kota Asal</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Kota</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarKabupaten))
                                    @foreach ($dataPendaftarKabupaten as $data)
                                        <tr>
                                            <td>{{$data->Kabupaten}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div> --}}

                <div class="card mb-3 bg-light">
                    <div class="card-header">Berdasarkan Sumber Informasi</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Informasi</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarSumberInformasi))
                                    @foreach ($dataPendaftarSumberInformasi as $data)
                                        <tr>
                                            <td>{{$data->sumber_informasi}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mb-3 bg-light">
                    <div class="card-header">Pendaftar berdasarkan Petugas Pendaftaran</div>
                    <div class="card-body">
                        <table class="table table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th>Jurusan</th>
                                    <th>Jumlah</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($dataPendaftarPetugasPendaftaran))
                                    @foreach ($dataPendaftarPetugasPendaftaran as $data)
                                        <tr>
                                            <td>{{$data->petugas_pendaftaran}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
           
        </div>
    </div>
@endsection