
<br>
<div class="row">
    <div class="col-md-3">
		<div class="row">
			<div class="card mb-3" style="width:100%">
				<div class="card-header">
					@if(isset($kelas))
					<h5 class="card-title">Pendaftaran Kelas TK {{$kelas}}</h5>
					@endif
				</div>
				<div class="card-body">
					@if(isset($kelas))
						@if($kelas=='A')
							<p>Berusia minimal 4 tahun pada bulan Juni 2019</p><p>Tanggal kelahiran anak tidak lebih dari 30 Juni 2015</p>
						@else
							<p>Berusia minimal 5 tahun pada bulan Juni 2019</p><p>Tanggal kelahiran anak tidak lebih dari 30 Juni 2014</p>
						@endif
					@endif
				</div>
			</div>
		</div>
    </div>

    <div class="col-md-9">
		<form action="{{url('/daftar')}}" method="POST" role="form">
			{{ csrf_field() }}
			@if(isset($kelas))
			<input type="hidden" name="class" class="form-control" value="{{$kelas}}">
			@endif
			@if(isset($code))
			<input type="hidden" name="code" class="form-control" value="{{$code}}">
			@endif
			
			<input type="hidden" name="kategori" value="Umum">

			<div class="card mb-3" style="width:100%">
				<div class="card-header">
					<h5 class="card-title">Data Anak</h5>
				</div>
				<div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Nama Lengkap</label>
                        <div class="col-md-9">
							<input 
							    type="text" placeholder="Nama Lengkap" name="nama" id="nama" class="form-control form-control-sm" 
							    value="{{old('name')}}" required="required" title="Nama Lengkap" onkeyup="this.value = this.value.toUpperCase()">
        						@if ($errors->all())
		  							<div class="alert alert-warning">
	        							<strong>{{ $errors->first('name') }}</strong>
			        				</div>
								@endif
			       				<div id='name-validation'></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Jenis Kelamin</label>
                        <div class="col-md-9 radio" required="required">
							<label class="col-form-label col-form-label-sm">
								<input type="radio" name="gender" value="L"> Laki-laki
							    &emsp;
								<input type="radio" name="gender" value="P"> Perempuan
							</label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Tempat & Tanggal Lahir</label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-3">
                                    <input 
                                        type="text" class="form-control form-control-sm" name="birth_place" placeholder="Tempat lahir" 
                                        value="{{old('birth_place')}}" required="required" title="Tempat Lahir">
                                </div>

                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                        @if(isset($kelas))
                                            @if($kelas=='A')
											<select name="tahun" class="form-control form-control-sm" required
											data-periode="{{$periode}}">
											    <option>Tahun</option>
												<option value="2013">2013</option>
												<option value="2014">2014</option>
												<option value="2015">2015</option>
                                            </select>
                                            @else
											<select name="tahun" class="form-control form-control-sm" required
											data-periode="{{$periode}}">
											    <option>Tahun</option>
												<option value="2012">2012</option>
												<option value="2013">2013</option>
												<option value="2014">2014</option>
                                            </select>
                                            @endif
                                        @endif
                                        </div>
                                    </div>
                                </div>
                                            
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
											<select name='bulan' class="form-control form-control-sm" required
											data-periode="{{$periode}}">
												<option>Bulan</option>
												<option value='1'>Januari</option>
												<option value='2'>Februari</option>
												<option value='3'>Maret</option>
												<option value='4'>April</option>
												<option value='5'>Mei</option>
												<option value='6'>Juni</option>
												<option value='7'>Juli</option>
												<option value='8'>Agustus</option>
												<option value='9'>September</option>
												<option value='10'>Oktober</option>
												<option value='11'>November</option>
												<option value='12'>Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <script>
									$('select[name="tahun"]').on("change", function() {
										console.log("test");
										var tahun = parseInt($(this).val());
										var periode = $(this).data("periode");
										var periodeExp = periode.split("-");
										var periodeTahun = parseInt(periodeExp[0]);
										console.log(tahun , periodeTahun);
                                        @if(isset($kelas))
                                            @if($kelas=='A')
        										if(tahun == (periodeTahun-4)){
                                            @else
        										if(tahun == (periodeTahun-5)){
                                            @endif
                                        @endif
					
											$('select[name="bulan"]').empty();
											$('select[name="bulan"]').append(
											   "<option>Bulan</option>"+
											   "<option value='1'>Januari</option>"+
												"<option value='2'>Februari</option>"+
												"<option value='3'>Maret</option>"+
												"<option value='4'>April</option>"+
												"<option value='5'>Mei</option>"+
												"<option value='6'>Juni</option>"
											);
										}
									});
								</script>
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col-md-12">
											<select name='hari' class="form-control form-control-sm" required>
												<option value="">
													Hari
												</option>
												@for($i=1; $i<=31; $i++)
													<option value="{{$i}}">{{$i}}</option>
												@endfor
											</select>                                           
                                        </div>
                                    </div>
								</div>
								<script>
									$('select[name="bulan"]').on("change", function() {
										var lastday = function(y,m){
											return  new Date(y, m +1, 0).getDate();
										}
										
										var tahun = $('select[name="tahun"]').val();
										var bulan = parseInt($(this).find(":selected").val());
										var periode = $(this).data("periode");
										var periodeExp = periode.split("-");
										var periodeTahun = parseInt(periodeExp[0]);
										var periodeBulan = parseInt(periodeExp[1]);
										var limit = lastday(periodeTahun,bulan-1);
										console.log(periodeTahun, periodeBulan);
										$('select[name="hari"]').empty();
										for($i=1; $i<=limit; $i++){
											$('select[name="hari"]').append(
												"<option value="+$i+">"+$i+"</option>"
											);
										}
										
									});
								</script>
                            </div>
						<!-- <input type="date" name="birth_date" id="date" class="form-control" value="{{old('birth_date')}}" required="required" title="Tanggal Lahir"> -->
						@if ($errors->all())
							<div class="alert alert-warning">
								<strong>{{ $errors->first('date') }}</strong>
							</div>
						@endif
						<div id='date-validation'></div>
						<div id='age-validation'></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Anak ke</label>
                        <div class="col-md-2">
                            <input 
                                type="number" class="form-control form-control-sm" name="anakke" value="{{old('anakke')}}" required>
                        </div>
                        <label class="col-md-2 col-form-label col-form-label-sm">dari</label>
                        <div class="col-md-2">
                            <input 
                                type="number" class="form-control form-control-sm" name="anakdari" value="{{old('anakdari')}}" required>
                        </div>
                        <label class="col-md-2 col-form-label col-form-label-sm">bersaudara</label>
                    </div>

                    
                </div>
            </div>

			<div class="card mb-3" style="width:100%">
				<div class="card-header">
					<h5 class="card-title">Data Orangtua</h5>
				</div>
				<div class="card-body">

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Nama Ayah Kandung</label>
                        <div class="col-md-9">
							<input 
							    type="text" placeholder="Nama Ayah Kandung" name="namaortu1" id="namaortu1" class="form-control form-control-sm" 
							    value="{{old('namaortu1')}}" required="required" onkeyup="this.value = this.value.toUpperCase()">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Nama Ibu Kandung</label>
                        <div class="col-md-9">
							<input 
							    type="text" placeholder="Nama Ibu Kandung" name="namaortu2" id="namaortu2" class="form-control form-control-sm" 
							    value="{{old('namaortu2')}}" required="required" onkeyup="this.value = this.value.toUpperCase()">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Nama Wali <i>(jika ada)</i></label>
                        <div class="col-md-9">
							<input 
							    type="text" placeholder="Nama Wali" name="namawali" id="namawali" class="form-control form-control-sm" 
							    value="{{old('namawali')}}" onkeyup="this.value = this.value.toUpperCase()">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">Alamat Lengkap</label>
                        <div class="col-md-9">
							<textarea name="alamat" class="form-control form-control-sm" rows="3" id="comment" placeholder="Alamat lengkap" required>{{old('alamat')}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">No Handphone 1</label>
                        <div class="col-md-9">
							<input 
							    type="number" placeholder="No Handphone 1" name="phone_number" id="phoneNumber" class="form-control form-control-sm" 
							    value="{{old('phone_number')}}" required="required" title="No Handphone">
							    @if ($errors->all())
									<div class="alert alert-warning">
										<strong>{{ $errors->first('phone_number') }}</strong>
									</div>
								@endif
								<div id='phone-validation'></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label col-form-label-sm">No Handphone 2</label>
                        <div class="col-md-9">
							<input 
							    type="number" placeholder="No Handphone 2" name="tlp2" id="phoneNumber2" class="form-control form-control-sm" 
							    value="{{old('tlp2')}}"title="No Handphone 2">
                        </div>
                    </div>
                </div>
            </div>

					<div class="form-group row">
						<div class="checkbox">
							<label class="col-md-12 col-form-label col-form-label-sm">
								<input type="checkbox" value="" name="chk" id="check">
								 Data yang saya masukan di atas sudah benar (Centang)
							</label>
						</div>
					</div>

			<button type="submit" id="btnD" class="btn btn-danger btn-block">Submit</button>
        </form>
    </div>
</div>



{{--
<div class="" style="padding-top: 15px; padding-bottom: 100px;">
	<div class="col-md-8 offset-md-2">
		<div class="row">
			<div class="card mb-3" style="width:100%">
				<div class="card-header">
					<h5 class="card-title">Kelas Pendaftaran</h5>
				</div>
				<div class="card-body">
					@if(isset($kelas))
						<h4>Kelas {{$kelas}} / Kelompok <span id="kelas">{{$kelas}}</span></h4>
						
						@if($kelas=='A')
							<p>Berusia Minimal 4 Tahun (Pada bulan Juni 2019)</p>
						@else
							<p>Berusia Minimal 5 Tahun (Pada bulan Juni 2019)</p>
						@endif
					@endif
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-8 offset-md-2">
		<div class="row">
			<form action="{{url('/daftar')}}" method="POST" role="form" style="width:100%">
			{{ csrf_field() }}
			@if(isset($kelas))
			<input type="hidden" name="class" class="form-control" value="{{$kelas}}">
			@endif
			<input type="hidden" name="kategori" value="Umum">
				<div class="card mb-3">
					<div class="card-header">
						<h5 class="card-title">Identitas Anak</h5>
					</div>
					<div class="card-body">
								<div class="form-group">
									<label>Nama Lengkap (Sesuai Akte Lahir) <b style="color: red">*</b></label>
									
									<input type="text" placeholder="Nama Lengkap" 
										name="name" id="name" class="form-control" 
										value="{{isset($pendaftaran) ? $pendaftaran->nama : old('name') }}" 
										required="required" title="Nama Lengkap">
									
									@if ($errors->all())
										<div class="alert alert-warning">
											<strong>{{ $errors->first('name') }}</strong>
										</div>
									@endif
									<div id='name-validation'>
									</div>
								</div>
								
								<div class="form-group">
									<label>Jenis Kelamin<b style="color: red">*</b></label>
									<div class="radio" required="required">
										<label>
											<input type="radio" name="gender" value="L" 
											{{isset($pendaftaran) && $pendaftaran->kelamin == 'L' ? 'checked="checked"' : '' }}">
											Laki-laki
										</label>
										<label>
											<input type="radio" name="gender" value="P"
											{{isset($pendaftaran) && $pendaftaran->kelamin == 'P' ? 'checked="checked"' : '' }}">
											Perempuan
										</label>
									</div>
								</div>
								<div class="form-group">
									<label>Tempat Lahir<b style="color: red">*</b></label>
									<input type="text" name="birth_place" class="form-control"
									 value="{{isset($pendaftaran) ? $pendaftaran->tempat_lahir : old('birth_place')}}" 
									 required="required" title="Tempat Lahir">
								</div>
								
								
								
								<div class="form-group">
									<label>Tanggal Lahir <b style="color: red">*</b></label>
									<div class="row">
										<div class="col-md-4">
											<input type="number" min="0" max="31" name="tanggal" id="tanggal" class="form-control" value="{{old('date')}}" required="required" placeholder="Tanggal" title="">
										</div>
										<div class="col-md-4">
											<select name="bulan" id="bulan" class="form-control" required="required">
												<option value="">Bulan</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
										<div class="col-md-4">
											<input type="number" min="2011" max="2018" name="tahun" id="tahun" class="form-control" value="{{old('year')}}" required="required" placeholder="Tahun" title="">
										</div>
									</div>
									<!-- <input type="date" name="birth_date" id="date" class="form-control" value="{{old('birth_date')}}" required="required" title="Tanggal Lahir"> -->
									@if ($errors->all())
										<div class="alert alert-warning">
											<strong>{{ $errors->first('date') }}</strong>
										</div>
									@endif
									<div id='date-validation'>
									</div>
									<div id='age-validation'>
									</div>
								</div>
								
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>Anak Ke-</label>
											<input type="number" name="anakke" class="form-control" 
											value="{{isset($pendaftaran) ? $pendaftaran->anakke : old('anakke')}}" required="required" pattern="" title="">
										</div>
										<div class="col-md-6">
											<label>Dari - Bersaudara</label>
											<input type="number" name="anakdari" class="form-control" 
											value="{{isset($pendaftaran) ? $pendaftaran->anakke : old('anakdari')}}" required="required" pattern="" title="">
										</div>
									</div>
								</div>
					</div>
				</div>
				<div class="card mb-3 card-default">
					<div class="card-header">
						<h5 class="card-title">Identitas Orang Tua</h5>
					</div>
					<div class="card-body">
							<div class="form-group">

								<label>Nama Ayah Kandung:<b style="color: red">*</b></label>
									<input type="text" value="{{isset($pendaftaran) ? $pendaftaran->namaortu1 : old('namaortu1')}}" name="namaortu1" class="form-control" id="penyakit" placeholder="Nama Ayah Kandung" required>
							</div>


							<div class="form-group">

								<label>Nama Ibu Kandung:<b style="color: red">*</b></label>
									<input type="text" value="{{isset($pendaftaran) ? $pendaftaran->namaortu2 : old('namaortu2')}}" name="namaortu2" class="form-control" placeholder="Nama Ibu Kandung" id="penyakit" required>
							</div>


							<div class="form-group">

								<label>Nama Wali <i>(jika ada)</i>:</label>
									<input type="text" name="namawali" 
									value="{{isset($pendaftaran) ? $pendaftaran->namawali : old('namawali')}}" class="form-control" id="penyakit">
							</div>


							<div class="form-group">

								<label>Alamat Lengkap :<b style="color: red">*</b></label>
									<textarea name="alamat" class="form-control" rows="5" id="comment" placeholder="Alamat lengkap" required>{{isset($pendaftaran) ? $pendaftaran->alamat : old('alamat')}}</textarea>
							</div>
							<div class="form-group">
									<label>No. HP (1):<b style="color: red">*</b></label>
									<input type="text" placeholder="Nomor HP" name="phone_number" id="phoneNumber" class="form-control" 
									value="{{isset($pendaftaran) ? $pendaftaran->tlp : old('phone_number')}}" required="required" title="Nomor HP">
									<p style="margin-top: 5px; font-style: italic;">Kami akan mengirimkan konfirmasi jika pendaftaran berhasil</p>
									@if ($errors->all())
										<div class="alert alert-warning">
											<strong>{{ $errors->first('phone_number') }}</strong>
										</div>
									@endif
									<div id='phone-validation'>
									</div>
							</div>

							<div class="form-group">

								<label>No. Tlp/HP (2):</label>
									<input type="text" name="tlp2" class="form-control" 
									value="{{isset($pendaftaran) ? $pendaftaran->tlp2 : old('tlp2')}}" id="penyakit">
							</div>

							<div class="form-group">

								<label>No. Tlp/HP (3):</label>
									<input type="text" name="tlp3" class="form-control" 
									value="{{isset($pendaftaran) ? $pendaftaran->tlp3 : old('tlp3')}}" id="penyakit">
							</div>
							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="" name="chk" id="check">
										Data yang saya masukan di atas adalah benar (Centang)
									</label>
								</div>
							</div>
					</div>
				</div>
				
				<div class="mt-3 col-md-12">
					<div class="form-group">
						
							<button type="submit" id="btnD" class="btn btn-success btn-block">Submit</button>
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
--}}
<script src="{{asset('public/js/input-validation.js?v=6')}}"></script>
<!-- <script type="text/javascript">
	$(document).ready(function() 
	{
	    $('#loader').hide();

	    $('form').submit(function() 
	    {
	        $('#loader').show();
	    }) 
	})
</script> -->
