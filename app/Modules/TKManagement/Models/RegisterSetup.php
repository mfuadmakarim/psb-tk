<?php

namespace App\Modules\TKManagement\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RegisterSetup extends Model {
	protected $table = 'setup';

    public function __construct() {
        $this->table = 'setup';
    }

    public function getSetup() {
        $data = DB::table('setup')
                ->first();

        return $data;
    }
}