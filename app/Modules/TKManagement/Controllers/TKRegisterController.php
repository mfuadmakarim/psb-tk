<?php

namespace App\Modules\TKManagement\Controllers;

use DateTime;
use Validator;
use Alert;
use Illuminate\Http\Request;
use Laravolt\Indonesia\Facade;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Modules\TKManagement\Models\User;
use App\Modules\TKManagement\Models\UserTesting;
use App\Modules\TKManagement\Models\RegisterGelombang;
use App\Modules\TKManagement\Models\RegisterSetup;
// use App\Modules\CommonModule\Controllers\SmsGatewayController;
class TKRegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function getRegister() {
        // (new SmsGatewayController)->sendRegisterSuccessConfirmation("085920691077", 'Testing SMS');
        $title = "Pendaftaran Santri Baru TK Persis Tarogong";
        $gelombang = new RegisterGelombang;
        $setup = (new RegisterSetup)->getSetup();

        $datenow = new DateTime('now'); 
        
        $datestart = new DateTime($setup->tgl_pendaftaran_start);
        $datestart->setTime(08,00);

        
        $dateend = new DateTime($setup->tgl_pendaftaran_end);
        $dateend->setTime(12,00);

        $gelombangbydate = $gelombang->getGelombangByDate($datenow->format('Y-m-d'));

        if($datenow >= $datestart && $datenow <= $dateend && $gelombangbydate){
            $kelas = $gelombangbydate->kelas;
            return view('TKManagement::register.registerPage', array(
                'title' => $title,
                'kelas' => $kelas
            ));
        }
        else{
            if($datenow > $dateend){
                $status = 99; //ditutup
            }
            elseif($datenow < $datestart){
                $status = 98; //belum dibuka
            }
            return view('TKManagement::register.infoPage', array(
                'datenow' => $datenow->format('Y-m-d'),
                'datestart' => $datestart->format('Y-m-d'),
                'dateend' => $dateend->format('Y-m-d'),
                'title' => $title,
                'status' => $status,
            ));            
        }
    }

    public function getRegisterTesting() {
        // (new SmsGatewayController)->sendRegisterSuccessConfirmation("085920691077", 'Testing SMS');
        $title = "Pendaftaran Santri Baru TK Persis Tarogong";
        $gelombang = new RegisterGelombang;
        $setup = (new RegisterSetup)->getSetup();

        $datenow = new DateTime('now'); 
        
        $datestart = new DateTime($setup->tgl_pendaftaran_start);
        $datestart->setTime(08,00);

        
        $dateend = new DateTime($setup->tgl_pendaftaran_end);
        $dateend->setTime(12,00);

        $gelombangbydate = $gelombang->getGelombangByDate($datenow->format('Y-m-d'));


        if($datenow >= $datestart && $datenow <= $dateend && $gelombangbydate){
            $kelas = $gelombangbydate->kelas;
            return view('TKManagement::register.registerPage', array(
                'title' => $title,
                'kelas' => $kelas
            ));
        }
        else{
            if($datenow > $dateend){
                $status = 99; //ditutup
            }
            elseif($datenow < $datestart){
                $status = 98; //belum dibuka
            }
            return view('TKManagement::register.infoPage', array(
                'datenow' => $datenow->format('Y-m-d'),
                'datestart' => $datestart->format('Y-m-d'),
                'dateend' => $dateend->format('Y-m-d'),
                'title' => $title,
                'status' => $status,
            ));            
        }
    }

    public function getRegisterAsatidz() {
        // (new SmsGatewayController)->sendRegisterSuccessConfirmation("085920691077", 'Testing SMS');
        // $userCount = (new User)->getUserByKelas();
       
        // dd($userCount->count());
        // $title = "Pendaftaran Santri Baru TK Persis Tarogong";
        // $gelombang = new RegisterGelombang;
        // $setup = (new RegisterSetup)->getSetup();

        // $datenow = new DateTime();
        
        // $datestart = new DateTime($setup->tgl_pendaftaran_start);
        // $datestart->setTime(07,00);
    
        // $dateend = new DateTime($setup->tgl_pendaftaran_end);
        // $dateend->setTime(12,00);


        // // dd($datenow, $datestart, $dateend);
        // $gelombangbydate = $gelombang->getGelombangByDate($datenow->format('Y-m-d'));

        // if(($datenow > $datestart) && ($datenow < $dateend)){
        //     $kelas = $gelombangbydate->kelas;
        //     return view('TKManagement::register.registerPageAsatidz', array(
        //         'title' => $title,
        //         'kelas' => $kelas
        //     ));
        // }
        // else{
        //     if($datenow > $datestart){
        //         $status = 99;
        //     }
        //     elseif($datenow < $dateend){
        //         $status = 98;
        //     }
        //     return view('TKManagement::register.infoPageAsatidz', array(
        //         'datenow' => $datenow->format('Y-m-d'),
        //         'datestart' => $datestart->format('Y-m-d'),
        //         'dateend' => $dateend->format('Y-m-d'),
        //         'title' => $title,
        //         'status' => $status
        //     ));            
        // }
        echo "Tidak Tersedia";
    }

    public function postRegister(Request $request) {
        $userId = Auth::id();
    	$validator = Validator::make($request->all(), [
            'nama' => 'required|unique:pendaftaran',
            'kategori' => 'required',
            'phone_number' => 'required',
            'tahun' => 'required',
            'bulan' => 'required',
            'hari' => 'required',
            'gender' => 'required',
            'birth_place' => 'required',
            'anakke' => 'required',
            'anakdari' => 'required',
            'namaortu1' => 'required',
            'namaortu2' => 'required',
            'alamat' => 'required'
        ]);
        
        // $birthDate = $request->input('provinceId')."/".$request->input('cityId')."/".$request->input('districtId');
        // dd($birthDate);
        $tahun = $request->input('tahun');
        $bulan = $request->input('bulan');
        $hari = $request->input('hari');

        $tanggal_lahir = $tahun."-".$bulan."-".$hari;
        $userDate = (new User)->isDateExist($tanggal_lahir, $request->input('name'));
        // if ($userDate) {
        //     alert()->warning('Nama dengan tanggal lahir yang sama sudah terdaftar', 'oops')->persistent('Tutup');
        //     return Redirect::back()->withInput()->withErrors($validator);
        //     exit();
        // }
        
        $date = new DateTime($request->input('date'));
        $kelas = $request->input('kelas');
        
        $setup = (new RegisterSetup)->getSetup();
        $setupDate = new DateTime($setup->tgl_periode);
        $interval = $date->diff($setupDate)->format('%y'); 

        if($kelas == 'A'){
            if($interval < 4){
                alert()->warning('Usia tidak sesuai ketentuan', 'oops')->persistent('tutup');
                return Redirect::back()->withInput();
            }
        }elseif($kelas == 'B'){
            if($interval < 5){
                alert()->warning('Usia tidak sesuai ketentuan', 'oops')->persistent('tutup');
                return Redirect::back()->withInput();
            }
        }
                
        // dd($userDate);

        // $userPhoneNumber = (new User)->isPhoneNumberExist($request->input('phone_number'));
        // if ($userPhoneNumber) {
        //         alert()->warning('Nomor HP sudah terdaftar', 'oops')->persistent('tutup');
        //         return Redirect::back()->withInput();
        // }


        if ($validator->fails()) {
                return Redirect::back()->withInput()->withErrors($validator);
        }

        //Save data
        $user = new User;
        $user->nama = $request->input('nama');
        $user->user_id = $userId;
        $user->kategori = $request->input('kategori');
        $user->tlp = $request->input('phone_number');
        $user->tanggal_lahir = $tanggal_lahir;
        $user->kelas = $request->input('class');
        $user->kelamin = $request->input('gender');
        $user->tempat_lahir = $request->input('birth_place');
        $user->anakke = $request->input('anakke');
        $user->anakdari = $request->input('anakdari');
        $user->namaortu1 = $request->input('namaortu1');
        $user->namaortu2 = $request->input('namaortu2');
        $user->namawali = $request->input('namawali');
        $user->alamat = $request->input('alamat');
        $user->tlp2 = $request->input('tlp2');
        $user->tlp3 = $request->input('tlp3');
        $user->save();
        $kls = $request->input('class');
        $pendaftaran = DB::table('pendaftaran')->where('kelas', '=', $kls)->get();
        
        $userCount = $pendaftaran->count();
        
        if($request->input('code')){
            $quota = (new RegisterGelombang)->getGelombangByKelas($request->input('class'))->kuota_sesi1;
        }else{
            $quota = (new RegisterGelombang)->getGelombangByKelas($request->input('class'))->kuota_sesi2;
        }
        if($userCount > $quota){
                return redirect('/daftar/failed/'.$user->id.'?name=' . $request->input('nama'));
        }else{
                return redirect('/daftar/success/'.$user->id.'?name=' . $request->input('nama'));
                
        }
       
    }

    // public function postRegisterTesting(Request $request) {

    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'kategori' => 'required',
    //         'phone_number' => 'required',
    //         'districtId' => 'required',
    //         'cityId' => 'required',
    //         'provinceId' => 'required',
    //         'gender' => 'required',
    //         'birth_place' => 'required',
    //         'anakke' => 'required',
    //         'anakdari' => 'required',
    //         'namaortu1' => 'required',
    //         'namaortu2' => 'required',
    //         'alamat' => 'required'
    //     ]);
        
    //     $birthDate = $request->input('provinceId')."/".$request->input('cityId')."/".$request->input('districtId');
    //     $userDate = (new UserTesting)->isDateExist($birthDate, $request->input('name'));
    //     if ($userDate) {
    //         alert()->warning('Nama dengan tanggal lahir yang sama sudah terdaftar', 'oops')->persistent('Tutup');
    //         return Redirect::back()->withInput()->withErrors($validator);
    //         exit();
    //     }

    //     $date = new DateTime($request->input('date'));
    //     $kelas = $request->input('kelas');
        
    //     $setup = (new RegisterSetup)->getSetup();
    //     $setupDate = new DateTime($setup->tgl_periode);
    //     $interval = $date->diff($setupDate)->format('%y'); 

    //     if($kelas == 'A'){
    //         if($interval < 4){
    //             alert()->warning('Usia tidak sesuai ketentuan', 'oops')->persistent('tutup');
    //             return Redirect::back()->withInput();
    //             exit();
    //         }
    //     }elseif($kelas == 'B'){
    //         if($interval < 5){
    //             alert()->warning('Usia tidak sesuai ketentuan', 'oops')->persistent('tutup');
    //             return Redirect::back()->withInput();
    //             exit();
    //         }
    //     }
                
    //     // $userPhoneNumber = (new User)->isPhoneNumberExist($request->input('phone_number'));
    //     // if ($userPhoneNumber) {
    //     //         alert()->warning('Nomor HP sudah terdaftar', 'oops')->persistent('tutup');
    //     //         return Redirect::back()->withInput();
    //     // }


    //     if ($validator->fails()) {
    //             return Redirect::back()->withInput()->withErrors($validator);
    //     }
    //     //Save data
    //     $user = new UserTesting;
    //     $user->nama = $request->input('name');
    //     $user->kategori = $request->input('kategori');
    //     $user->tlp = $request->input('phone_number');
    //     $user->tanggal_lahir = $tanggal_lahir;
    //     $user->kelas = $request->input('class');
    //     $user->kelamin = $request->input('gender');
    //     $user->tempat_lahir = $request->input('birth_place');
    //     $user->anakke = $request->input('anakke');
    //     $user->anakdari = $request->input('anakdari');
    //     $user->namaortu1 = $request->input('namaortu1');
    //     $user->namaortu2 = $request->input('namaortu2');
    //     $user->namawali = $request->input('namawali');
    //     $user->alamat = $request->input('alamat');
    //     $user->tlp2 = $request->input('tlp2');
    //     $user->tlp3 = $request->input('tlp3');
    //     $user->save();
        
    //     $kls = $request->input('class');
    //     $pendaftaran = DB::table('pendaftaran')->where('kelas', '=', $kls)->get();
        
    //     $userCount = $pendaftaran->count();
        
    //     $quota = (new RegisterGelombang)->getGelombangByKelas($request->input('class'))->kuota;
    //     if($userCount > $quota){
    //             return redirect('/daftar/failed/'.$user->id.'?name=' . $request->input('name'));
    //     }else{
    //             return redirect('/daftar/success/'.$user->id.'?name=' . $request->input('name'));
                
    //      }
         
       
    // }

    public function postRegisterAsatidz(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'kategori' => "required",
            'phone_number' => 'required',
            'districtId' => 'required',
            'cityId' => 'required',
            'provinceId' => 'required',
            'gender' => 'required',
            'birth_place' => 'required',
            'anakke' => 'required',
            'anakdari' => 'required',
            'namaortu1' => 'required',
            'namaortu2' => 'required',
            'alamat' => 'required'
        ]);
        
        $birthDate = $request->input('provinceId')."/".$request->input('cityId')."/".$request->input('districtId');
        // dd($birthDate);
        // $userDate = (new User)->isDateExist($birthDate, $request->input('name'));
        // if ($userDate) {
        //     alert()->warning('Nama dengan tanggal lahir yang sama sudah terdaftar', 'oops')->persistent('Tutup');
        //     return Redirect::back()->withInput()->withErrors($validator);
        //     exit();
        // }

        $date = new DateTime($request->input('date'));
        $kelas = $request->input('kelas');
        
        $setup = (new RegisterSetup)->getSetup();
        $setupDate = new DateTime($setup->tgl_periode);
        $interval = $date->diff($setupDate)->format('%y'); 

        if($kelas == 'A'){
            if($interval < 4){
                alert()->warning('Usia tidak sesuai ketentuan', 'oops')->persistent('tutup');
                return Redirect::back()->withInput();
                exit();
            }
        }elseif($kelas == 'B'){
            if($interval < 5){
                alert()->warning('Usia tidak sesuai ketentuan', 'oops')->persistent('tutup');
                return Redirect::back()->withInput();
                exit();
            }
        }
                
        // dd($userDate);

        // $userPhoneNumber = (new User)->isPhoneNumberExist($request->input('phone_number'));
        // if ($userPhoneNumber) {
        //         alert()->warning('Nomor HP sudah terdaftar', 'oops')->persistent('tutup');
        //         return Redirect::back()->withInput();
        // }


        if ($validator->fails()) {
                return Redirect::back()->withInput()->withErrors($validator);
        }

        $tanggal_lahir = $request->input('provinceId')."/".$request->input('cityId')."/".$request->input('districtId');
       
        //Save data
        $user = new User;
        $user->nama = $request->input('name');
        $user->kategori = $request->input('kategori');
        $user->tlp = $request->input('phone_number');
        $user->tanggal_lahir = $tanggal_lahir;
        $user->kelas = $request->input('class');
        $user->kelamin = $request->input('gender');
        $user->tempat_lahir = $request->input('birth_place');
        $user->anakke = $request->input('anakke');
        $user->anakdari = $request->input('anakdari');
        $user->namaortu1 = $request->input('namaortu1');
        $user->namaortu2 = $request->input('namaortu2');
        $user->namawali = $request->input('namawali');
        $user->alamat = $request->input('alamat');
        $user->tlp2 = $request->input('tlp2');
        $user->tlp3 = $request->input('tlp3');
        $user->save();
        
        $kls = $request->input('class');
        $pendaftaran = DB::table('pendaftaran')->where('kelas', '=', $kls)->get();
        $userCount = $pendaftaran->count();
        $quota = (new RegisterGelombang)->getGelombangByKelas($request->input('class'))->kuota;
        // dd($request->input('class'));
        // dd($quota);
        if($userCount > $quota){
                return redirect('/daftar/failed/'.$user->id.'?name=' . $request->input('name'));
        }else{
                return redirect('/daftar/success/'.$user->id.'?name=' . $request->input('name'));
                
         }
       
    }

    public function registerSuccess($id, Request $request) {
        $status = "Berhasil";
        DB::table('pendaftaran')
        ->where("pendaftaran.id", '=',  $id)
        ->update(array('pendaftaran.status'=> $status));

    	$title = "Sukses!";
    	$name = $request->input('name');
        $user = User::select('id', 'nama', 'tlp', 'kelas')->where('id', $id)->first();
        $message = 'Pendaftaran santri baru TK Persis Tarogong ('.$user->kelas.') nomor urut ' .$user->id. ' atas nama ' .$user->nama. ' Berhasil.';
        $telepon = $user->tlp;
        // (new SmsGatewayController)->sendRegisterSuccessConfirmation($telepon, $message);
        return view('TKManagement::register.successPage', array(
            'name' => $name,
            'title' => $title,
            'no' => $id
        ));
    }

    public function registerFailed($id, Request $request) {
        $status = "Pending";
        DB::table('pendaftaran')
        ->where("pendaftaran.id", '=',  $id)
        ->update(array('pendaftaran.status'=> $status));

        $title = 'Gagal';
        $user = User::select('id', 'nama', 'tlp')->where('id', $id)->first();
        $message = 'Pendaftaran TK Persis Tarogong tidak berhasil karena jumlah pendaftar telah mencapai kuota';
        $telepon = $user->tlp;
        // (new SmsGatewayController)->sendRegisterSuccessConfirmation($telepon, $message);
        return view('TKManagement::register.failedPage', array( 
            'title' => $title
        ));
            
    }

    public function checkIsPhoneExist(Request $request) {

        $phone = $request->input("phone");

        $response = array(
            'status' => '0',
        );
        //check if email is already used
        $userPhone = (new User)->isPhoneNumberExist($phone);

        if ($userPhone) {
            $response = array(
                'status' => '1',
                'code' => "test",
            );
        } else {
            $response = array(
                'status' => '0',
            );
        }

        return Response::json($response);
    }

     //
    public function checkIsNameExist(Request $request) {
        $name = $request->input('name');
        $response = array(
            'status' => '0',
        );
        $userName = (new User)->isNameExist($name);
        if ($userName) {
            $response = array(
                'status' => '1',
            );
        } else {
            $response = array(
                'status' => '0',
            );
        }

        return Response::json($response);
    }

    public function checkIsDateExist(Request $request) {
        $date = $request->input('date');
        $name = $request->input('name');
        $response = array(
            'status' => '0',
        );
        $userDate = (new User)->isDateExist($date, $name);
        if ($userDate) {
            $response = array(
                'status' => '1',
            );
        } else {
            $response = array(
                'status' => '0',
            );
        }

        return Response::json($response);
    }

    public function checkAge(Request $request){
        $date = new DateTime($request->input('date'));
        $kelas = $request->input('kelas');
        $response = array(
            'status' => '0',
        );
        $setup = (new RegisterSetup)->getSetup();
        $setupDate = new DateTime($setup->tgl_periode);
        $interval = $date->diff($setupDate)->format('%y'); 
        if($kelas == 'A'){
            if($interval < 4){
                $response = array(
                    'status' => '1',
                );
            }else{
                $response = array(
                    'status' => '0',
                );
            }
        }else{
            if($interval < 5){
                $response = array(
                    'status' => '1',
                );
            }else{
                $response = array(
                    'status' => '0',
                );
            }
        }
        return Response::json($response);
    }
    public function getProvinces(){
        return Facade::allProvinces();
    }
    public function getCities($provinceId){
        return Facade::findProvince($provinceId, 'cities');
    }
    public function getDistricts($cityId){
        return Facade::findCity($cityId, 'districts');
    }
    public function getVillages($districtId){
        return Facade::findDistrict($districtId, 'villages');
    }
}