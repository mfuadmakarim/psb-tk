<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftarMlnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftar_mln', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_lengkap')->nullable();
            $table->string('nisn')->nullable();
            $table->string('tahun_lulus')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->integer('anak_ke')->nullable(); 
            $table->integer('dari_bersaudara')->nullable();
            $table->string('sekolah_asal')->nullable();
            $table->string('alamat_sekolah_asal')->nullable();
            $table->string('kategori_sekolah_asal')->nullable();
            $table->string('rencana_tinggal')->nullable();
            $table->string('dorongan_masuk')->nullable();
            $table->string('sumber_informasi')->nullable();
            $table->string('pilihan_jurusan_1')->nullable();
            $table->string('pilihan_jurusan_2')->nullable();
            $table->string('pilihan_jurusan_3')->nullable();
            $table->string('nama_ayah')->nullable();
            $table->string('pendidikan_ayah')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->string('pendidikan_ibu')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('nama_wali')->nullable();
            $table->string('pendidikan_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->string('hubungan_wali_anak')->nullable();
            $table->string('penghasilan_orangtua')->nullable();
            $table->string('penanggung_biaya')->nullable();
            $table->string('alamat_lengkap')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kabupaten')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('no_hp_1')->nullable();
            $table->string('no_hp_2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftar_mln');
    }
}
