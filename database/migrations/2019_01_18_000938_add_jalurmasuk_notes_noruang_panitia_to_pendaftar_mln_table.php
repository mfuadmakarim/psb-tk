<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJalurmasukNotesNoruangPanitiaToPendaftarMlnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendaftar_mln', function($table) {
            $table->string('jalur_masuk')->nullable();
            $table->string('no_tes')->nullable();
            $table->string('no_ruang')->nullable();
            $table->string('panitia_penerima')->nullable();
            $table->boolean('is_admin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pendaftar_mln', function($table) {
            $table->dropColumn('jalur_masuk');
            $table->dropColumn('no_tes');
            $table->dropColumn('no_ruang');
            $table->dropColumn('panitia_penerima');
            $table->dropColumn('is_admin');
        });
    }
}
