@extends('Layouts::layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 offset-md-4"><br><img src="{{asset('http://psb.persistarogong.com/wp-content/uploads/2018/11/weblogo-persista-2.png')}}" width="100%"/><br><br>
                    <center>Silakan <b><a href="{{ route('register') }}">BUAT AKUN</a></b> terlebih dahulu<br><br></center>
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center"><b>LOGIN PSB TK PERSIS</b></div>
                <form class="form-horizontal" method="POST" action="{{url('/auth/check')}}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('no_wa') ? ' has-error' : '' }}">
                            <label for="no_wa" class="col-12 control-label">No HP</label>

                            <div class="col-12">
                                <input id="no_wa" type="number" class="form-control" name="no_wa" value="{{ old('no_wa') }}" required autofocus>

                                @if ($errors->has('no_wa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_wa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-12 control-label">Password</label>

                            <div class="col-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-danger btn-block">
                                    Login
                                </button>

                                {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Lupa Password?
                                </a> --}}
                                <a class="btn btn-link" href="{{ route('register') }}">
                                    Buat Akun
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
