@extends('CommonModule::layouts.layout')
@section('content')
<div class="container" style="padding-top: 15px; padding-bottom: 100px;">
	<div class="col-md-6 col-md-offset-3">
		<div class="row" style="margin-bottom: 10px">
			<div class="col-md-12 text-center">
					<img width="100" src="{{asset('public/images/logo.png')}}">
					<h3>Pendaftaran Santri Baru TK Persis Tarogong</h3>
			</div>
		</div>
		<div class="row">
			<div class="alert alert-info text-center">
				<!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
				@if($status=='98')
					<h3 style="margin-top: 0;">Pendaftaran Belum Dibuka</h4>
				@elseif($status=='99')
					<h3 style="margin-top: 0;">Pendaftaran Ditutup</h4>
				@endif
				<h5>Tanggal Pendaftaran:<br></h5>
					<p>1 Maret 2019 (Kelas A)</p>
					<p>1 Maret 2019 (Kelas B)</p>
					
			</div>
		</div>
	</div>
</div>
@endsection