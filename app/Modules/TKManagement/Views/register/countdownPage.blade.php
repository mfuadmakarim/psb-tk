@extends('Layouts::layout')
@section('content')
<div class="d-block p-5 text-center">
<h5>Pendaftaran Santri Baru TK Persis Akan Dibuka Pada 
    <span class="text-success font-weight-bold">{{Carbon\Carbon::parse($datestart)->format('d M Y')}}</span>
</h5>
<div class="d-none" id="date">{{$datestart}}</div>
<div class="alert alert-info d-table mx-auto mt-5" id="countdown" style="
    font-size: 48px;
"></div>
</div>
<script type="text/javascript">
    var date = $("#date").html();
    console.log(date);
    $("#countdown").countdown(date, function(event) {
        $(this).text(event.strftime('%d Hari %H:%M:%S'));
    });
</script>
@endsection