<?php

namespace App\Modules\Auth\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct()
    {
       Auth::logout();
    }

    public function index(){
    	return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $no_wa = $request->input('no_wa');
        $password = $request->input('password');
        if (Auth::attempt(['no_wa' => $no_wa, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('/');
        }else {
        return 'Password yang anda masukkan salah, silakan coba lagi';
    }

    }
}
