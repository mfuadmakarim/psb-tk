<?php
Route::group(['namespace' => 'App\Modules\User\Controllers', 'middleware' => 'web'], function() {

    //Home
    Route::get('/', 'CheckController@check');
    Route::get('/home', 'UserController@index');
    Route::get('/provinces', 'UserController@getProvinces');
    Route::get('/cetak-kartu/{id}', 'UserController@cetakKartu');
    Route::get('/cetak-kwitansi/{id}', 'UserController@cetakKwitansi');
    //Get Ajax
    Route::get('getProvince', 'UserController@getProvince');
    Route::get('getCities/{id}', 'UserController@getCities');
    Route::get('getDistricts/{id}', 'UserController@getDistricts');
    Route::get('getVillages/{id}', 'UserController@getVillages');
    Route::get('reg', 'UserController@registrasi');

    
 });
 Route::group(['namespace' => 'App\Modules\User\Controllers'], function() {
    Route::get('countdown', 'CountdownController@index');
    Route::get('registrasi', 'CheckController@registrasi');
    Route::get('closed', 'CheckController@closed');
 });