<link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet'>
<style>
body {
    font-family: 'Asap';font-size: 22px;
}
</style>

<table width="585" height="80"  border="0" style="font-size:12px; line-height: 1.4">
  <tr>
    <td width="12%" valign="top" ALIGN="right"><img src="{{asset('public/images/persistarogonglogo_flat.png')}}" height="68" /></td>
    <td width="37%" align="center" valign="middle"><b>PENERIMAAN SANTRI BARU<br><font style="font-size:14px">TK PERSIS TAROGONG</font><br>Tahun Pelajaran 2019-2020</b></td>
   <td width="5%" valign="top"></td>
   <td width="46%" align="center" valign="middle"><b>INFORMASI DAFTAR ULANG SANTRI BARU<br>
    </b><b>TK PERSIS TAROGONG<br>
    </td>
  </tr>
</table>

<table width="585" height="300" border="0" style="font-size:11px; line-height: 1.4">
  <tr>
    <td width="49%" colspan="4" align="center" valign="middle" bgcolor="#f44271">&nbsp;
      <span style="color:#FFF; font-size:13px"><B>KARTU TANDA PENDAFTARAN</B></span>
    </td>
    <td width="5%" rowspan="8" valign="top"></td>
    <td width="46%" height="30" colspan="3" align="center" bgcolor="#f44271"></td>
  </tr>
  <tr>
    <td colspan="4" height="90" align="center" valign="middle">Nama Peserta<br>
        <b style="font-size:14px; padding-left:10; padding-right:10">{{$dataCalonSantri->nama}}</b></td>
    <td rowspan="5" colspan="3" valign="top" align="center" style="font-size:10px; line-height:1.5">
        <p>
            <br>
            Pendaftar yang telah dinyatakan Berhasil, <br>
            <b>WAJIB melakukan daftar ulang</b> pada:<br>
            <b>Kamis-Jum'at, 7-8 Maret 2019<br>
            Pukul 08.30 - 12.00 WIB<br>
            di Ruang Serbaguna Pesantren Persis Tarogong</b><br></p>
        <p>
            Dengan menyerahkan persyaratan, berupa: <br>
            <b>2 lembar fotokopi akte lahir (dilegalisir), dan <br>
            2 lembar fotokopi kartu keluarga (dilegalisir)</b><br><br>
            Dan membayar biaya pendidikan
        </p>
    </td>
  </tr>
  <tr>
    <td height="25" width="18%" valign="middle" style="padding-left:8">No. Pendaftaran</td>
    <td width="2%" valign="middle">:</td>
    <td width="16%" valign="middle"><b style="font-size:13px;">PSBTK-Um-{{$dataCalonSantri->id}}</b></td>
    <td width="12%" valign="middle">Kelas : <b style="font-size:13px;">{{$dataCalonSantri->kelas}}</b></td>
  </tr>
  <tr>
    <td height="20" valign="top" style="padding-left:8" margin-left="10">Tempat, Tgl Lahir</td>
    <td valign="top">: </td>
    <td colspan="2" valign="top">{{$dataCalonSantri->tempat_lahir}} , {{$dataCalonSantri->tanggal_lahir}}</td>
  </tr>
  <tr>
    <td height="20" valign="top" style="padding-left:8; line-height:1.2" margin-left="10">Nama Ayah</td>
    <td valign="top">: </td>
    <td colspan="2" valign="top" style="line-height:1.2">
        {{$dataCalonSantri->namaortu1}} 
    </td>
  <tr>
    <td height="20" valign="top" style="padding-left:8; line-height:1.2" margin-left="10">Nama Ibu</td>
    <td valign="top">: </td>
    <td colspan="2" valign="top" style="line-height:1.2">
        {{$dataCalonSantri->namaortu2}} 
    </td>
  </tr>

  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td colspan="2" align="center" valign="middle" style="font-size:10px;">Garut, {{$dataCalonSantri->created_at}}<br>
      <br>
    Panitia</td>
    <td height="40" colspan="3" rowspan="2" valign="middle" align="center" bgcolor="#CCCCCC" style="padding-left:20; padding-right:10">
        <p style="font-size:9px;">KETERANGAN:<br>Kartu Tanda Pendaftaran ini harap dibawa dan <br>ditunjukkan kepada panitia saat Daftar ulang</p>
        <p style="font-size:9px;"><b>TIDAK DAFTAR ULANG DIANGGAP MENGUNDURKAN DIRI</b></p></td>

  </tr>
  <tr>
    <td height="25" colspan="4" valign="middle" bgcolor="#f44271"><div style="color:#FFF; font-size:10px" align="center"><strong>PESANTREN PERSATUAN ISLAM TAROGONG </strong></div></td>
  </tr>
</table>


<br>
<script>
  window.print();
</script>