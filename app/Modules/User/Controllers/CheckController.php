<?php

namespace App\Modules\User\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
use App\Modules\TKManagement\Models\User;
use App\Modules\TKManagement\Models\UserTesting;
use App\Modules\TKManagement\Models\RegisterGelombang;
use App\Modules\TKManagement\Models\RegisterSetup;
use Carbon\Carbon;
use DateTime;
class CheckController extends Controller
{
    public function registrasi(){
        $datenow = (new DateTime('now'))->format('Y-m-d');
        $gelombang = (new RegisterGelombang)->where('periode', $datenow)->first();
        $allGelombang = RegisterGelombang::all();

        $userId = Auth::id();
        $pendaftaran = (new User)->where('user_id', $userId)->first();
        // dd($gelombang);
        $sesi1_start = (new DateTime($gelombang->sesi1_start))->format('H:i:s');
        $sesi1_end = (new DateTime($gelombang->sesi1_end))->format('H:i:s');
        $kuota_sesi1 = $gelombang->kuota_sesi1;
        $sesi2_start = (new DateTime($gelombang->sesi2_start))->format('H:i:s');
        $sesi2_end = (new DateTime($gelombang->sesi2_end))->format('H:i:s');
        $kuota_sesi2 = $gelombang->kuota_sesi1;
        $kelas = $gelombang->kelas;
        $periode = $gelombang->periode;

        $timenow = (new DateTime('now'))->format('H:i:s');

        // dd($timenow, $sesi1_start, $sesi1_end, $sesi2_start, $sesi2_end);
        if($timenow < $sesi1_start){
            // dd('menunggu sesi 1');
            $statusSesi = 1;
            return view('TKManagement::register.infoPage', array(
                'allGelombang' => $allGelombang,
                'statusSesi' => $statusSesi,
                'kelas' => $kelas
            ));
        }elseif($timenow >= $sesi1_start && $timenow <= $sesi1_end){
            // dd('sesi 1 mulai');
            return redirect('/reg');
            // $statusSesi = 2;
            // return view('User::DashboardView', array(
            //     'statusSesi' => $statusSesi,
            //     'kelas' => $kelas,
            //     'periode' => $periode,
            //     'pendaftaran' => $pendaftaran,
            //     'code' => 'sesi1'
            // ));
        }elseif($timenow > $sesi1_end && $timenow < $sesi2_start){
            // dd('sesi 1 selesai, menunggu sesi 2');
            $statusSesi = 3;
            return view('TKManagement::register.infoPage', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas
            ));
        }elseif($timenow >= $sesi2_start && $timenow <= $sesi2_end){
            // dd('sesi 2 mulai');
            $statusSesi = 4;
            return view('User::DashboardView', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas,
                'periode' => $periode
            ));
        }elseif($timenow > $sesi2_end){
            // dd('sesi 2 selesai');
            $statusSesi = 5;
            return view('TKManagement::register.infoPage', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas
            ));
        }
    }
    public function check(){
        $status = $this->checkRegister();
        if($status == 'belum-buka'){
            return redirect('/countdown');
        }else if($status == 'buka'){
            return redirect('/registrasi');
        }else if($status == 'tutup'){
            return redirect('/closed');
        }
    }
    public function checkRegister(){
        $datenow = new DateTime('now');
        $setup = (new RegisterSetup)->getSetup();
        $datestart = new DateTime($setup->tgl_pendaftaran_start);
        $dateend = new DateTime($setup->tgl_pendaftaran_end);
        // dd($datenow, $datestart, $dateend);
        if($datenow < $datestart){
            return $status = 'belum-buka';
        }elseif($datenow >= $datestart && $datenow <= $dateend) {
            return $status = 'buka';
        }elseif($datenow > $dateend){
            return $status = 'tutup';
        }
    }
    public function closed(){
        return view('TKManagement::register.closePage');
    }
}
