@extends('Layouts::adminLayout')
@section('content')
<div class="container-fluid">
    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
    <div class="card bg-white mb-3 mt-3">
        <div class="card-header"><h5>Data Pembayaran</h5></div>
    
        <div class="card-body">
            <table id="listPembayaran" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Pendaftar</th>
                        <th>Jumlah Transfer</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataPembayaran as $data)
                    <tr>
                        <td scope="row">{{$data->pendaftar_id}}</td>
                        <td>{{$data->nama_lengkap}}</td>
                        <td>{{$data->jumlah_transfer}}</td>
                        <td>{{$data->tanggal_transfer}}</td>
                        
                        <td>
                            @if($data->status == 'Dibayar')
                                Dibayar
                            @else
                                <a href="{{url('admin/pembayaran/validasi/'.$data->id.'/'.$data->pendaftar_id)}}" class="btn btn-sm btn-primary">Validasi Pembayaran</a></td>
                            @endif
                        <td> <a href="{{url('cetak-kartu/'.$data->pendaftar_id)}}" class="btn btn-sm btn-success btn-block">Cetak Kartu Peserta</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection