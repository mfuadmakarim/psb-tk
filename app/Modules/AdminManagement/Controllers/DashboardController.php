<?php

namespace App\Modules\AdminManagement\Controllers;

use DateTime;
use Validator;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Modules\TKManagement\Models\User;
use App\Modules\TKManagement\Models\RegisterGelombang;
use App\Modules\TKManagement\Models\RegisterSetup;
use App\Modules\CommonModule\Controllers\SmsGatewayController;
class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getDashboard() {
    	$setup = RegisterSetup::all();
    	$gelombang = RegisterGelombang::all();
        $user = User::all();
        $total = count($user);
        $dataPendaftarA = (new User)->getCountKelasAStatus();
        $dataPendaftarB = (new User)->getCountKelasBStatus();
    	$title = "Admin TK Persis";
    	return view('AdminManagement::dashboard.dashboardPage', array(
            'title' => $title,
            'setup' => $setup,
            'gelombang' => $gelombang,
            'user' => $user,
            'dataPendaftarA' => $dataPendaftarA,
            'dataPendaftarB' => $dataPendaftarB,
            'total' => $total
        ));
    }

    public function updateSetup(Request $request){
    	$validator = Validator::make($request->all(), [
            'tgl_periode' => 'required',
            'tgl_pendaftaran_start' => 'required',
            'tgl_pendaftaran_end' => 'required'
        ]);
        if ($validator->fails()) {
        		alert()->warning('Data tidak lengkap', 'oops')->persistent('Tutup');
                return Redirect::back()->withInput()->withErrors($validator);
        }
        $setup = RegisterSetup::find($request->input('id'));
        $setup->tgl_periode = $request->input('tgl_periode');
        $setup->tgl_pendaftaran_start = $request->input('tgl_pendaftaran_start');
        $setup->tgl_pendaftaran_end = $request->input('tgl_pendaftaran_end');
        $setup->save();

        alert()->success('Data telah diupdate', 'Success')->persistent('Tutup');
        return redirect('/admin');
    }
    public function deleteUser(Request $request){
        $validator = Validator::make($request->all(), [
            'data' => 'required',
        ]);
        if ($validator->fails()) {
                alert()->warning('Pilih minimal 1 data', 'oops')->persistent('Tutup');
                return Redirect::back();
        }
        $datas = $request->data;
        foreach ($datas as $key => $value) {
             $user = User::where('id', $value)->delete();
        }
       
        alert()->success('Data telah dihapus', 'Success')->persistent('Tutup');
        return redirect('/admin');
    }
    public function updateGelombang(Request $request){
    	$validator = Validator::make($request->all(), [
            'kelas' => 'required',
            'periode' => 'required',
            'sesi1_start' => 'required',
            'sesi1_end' => 'required',
            'sesi2_start' => 'required',
            'sesi2_end' => 'required',
            'kuota_sesi1' => 'required',
            'kuota_sesi2' => 'required'
        ]);
        if ($validator->fails()) {
        		alert()->warning('Data tidak lengkap', 'oops')->persistent('Tutup');
                return Redirect::back()->withInput()->withErrors($validator);
        }
        $setup = RegisterGelombang::find($request->input('id'));
        $setup->kelas = $request->input('kelas');
        $setup->periode = $request->input('periode');
        $setup->sesi1_start = $request->input('sesi1_start');
        $setup->sesi1_end = $request->input('sesi1_end');
        $setup->sesi2_start = $request->input('sesi2_start');
        $setup->sesi2_end = $request->input('sesi2_end');
        $setup->kuota_sesi1 = $request->input('kuota_sesi1');
        $setup->kuota_sesi2 = $request->input('kuota_sesi2');
        $setup->kuota = ($request->input('kuota_sesi1') + $request->input('kuota_sesi2'));
        $setup->save();

        alert()->success('Data telah diupdate', 'Success')->persistent('Tutup');
        return redirect('/admin');
    }

    public function testToggle(Request $request){
        $value = $request->input("value");

        $response = array(
            'status' => '0',
        );
        
        if ($value==1) {
            $response = array(
                'status' => '1',
            );
        } else {
            $response = array(
                'status' => '0',
            );
        }

        return Response::json($response);
    }
}