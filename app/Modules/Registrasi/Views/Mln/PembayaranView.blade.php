
    <br>
    <h5>Pilih Metode Pembayaran</h5>
    <hr>
    <div class="row">
        <div class="col-md-8">
            Sebelum melakukan pembayaran, pastikan seluruh Data Pendaftar sudah terisi dengan benar dan lengkap. (Data Pendaftar tidak dapat diedit kembali jika pembayaran sudah divalidasi)<br><br>
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                1 - Transfer Bank (Verifikasi Manual)
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            
                            <ul>
                                <li><i>Pembayaran divalidasi mulai jam 08.00 WIB. Pembayaran akan divalidasi pada hari yang sama, jika pembayaran dan konfirmasi dilakukan sebelum pukul 15.00 WIB.</i></li>
                            @if($dataCalonSantri)
                                <li>Biaya pendaftaran sebesar <b class="text-success">Rp {{sprintf('%s', number_format(350000+$dataCalonSantri->id, 0))}}</b> 
                                ditambah 3 digit no unik untuk memudahkan pengecekan transaksi</li>
                            @endif
                                <li>Pembayaran transfer melalui:
                                    <ul>
                                        <li>
                                            <div class="group row">
                                                <div class="col-md-4">Bank</div>
                                                <div class="col-md-8"><b>BRI Syariah</b></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="group row">
                                                <div class="col-md-4">No Rekening</div>
                                                <div class="col-md-8"><b>1029357915</b></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="group row">
                                                <div class="col-md-4">A/n.</div>
                                                <div class="col-md-8"><b>Muhamad Pahad</b></div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li>Kirim bukti transfer via nomor <b>081573012345</b> (WA only)</li>
                                <li>Isi form Konfirmasi berikut</li>
                            </ul>
                <div class="col-md-12">
                    <strong>Konfirmasi Pembayaran via Bank Transfer</strong><br><br>
                </div>
                <form action={{url('/registrasi/mln/konfirmasi-pembayaran')}}  
                    method="POST" 
                    role="form" 
                    enctype="multipart/form-data" 
                    class="col-md-12">
                        @if($dataCalonSantri)
                        <input type="hidden" name="id" value="{{$dataCalonSantri->id}}"/>
                        @endif 
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Bank Pengirim</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    name="bank_pengirim" 
                                    class="form-control form-control-sm" 
                                    placeholder="Bank Pengirim" 
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Nama Akun Pengirim</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    name="nama_pengirim" 
                                    class="form-control form-control-sm" 
                                    placeholder="Nama Akun Pengirim" 
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">No Rekening Pengirim</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    name="norek_pengirim" 
                                    class="form-control form-control-sm" 
                                    placeholder="No Rekening" 
                                    required>
                            <i style="font-size:11px">Jika pembayaran melalui Teller Bank, isi dengan 'Teller'</i>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Bank Tujuan</label>
                            <div class="col-md-9">
                                <input 
                                    type="text" 
                                    name="bank_penerima" 
                                    class="form-control form-control-sm" 
                                    placeholder="Bank Tujuan"
                                    value="BRI Syariah"
                                    readonly
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Jumlah Transfer</label>
                            <div class="col-md-9">
                                <input 
                                    type="number" 
                                    name="jumlah_transfer" 
                                    class="form-control form-control-sm" 
                                    placeholder="Jumlah Transfer" 
                                    required>
                            <i style="font-size:11px">Isi hanya angka saja, tanpa titik atau koma. contoh 250999</i>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label col-form-label-sm">Tanggal Transfer</label>
                            <div class="col-md-9">
                                <input 
                                    type="date" 
                                    name="tanggal_transfer" 
                                    class="form-control form-control-sm" 
                                    placeholder="Tanggal Transfer" 
                                    required>
                            <i style="font-size:11px">Format tanggal dd/mm/yyyy. Contoh: 30/12/2000</i>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary float-right">Konfirmasi Pembayaran</button>
                </form>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2 - Transfer Virtual Account (Verifikasi Otomatis)
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            <ol>
                                <li>Pembayaran diverifikasi secara otomatis menggunakan <b>BNI Virtual Account</b></li>
                                <li>Setiap transaksi dikenakan biaya admin bank sebesar Rp 3.000,-</li>
                                <li>Kode Virtual Account akan muncul setelah klik "Generate VA"</li>
                            </ol>    
                            <button class="btn btn-primary float-right btn-block disabled">Generate VA</button>
                            <div align="center"><i style="font-size:80%">Mohon maaf sementara VA belum dapat digunakan</i></div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
