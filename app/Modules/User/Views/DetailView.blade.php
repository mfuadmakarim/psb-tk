<div id="detail" class="position-fixed w-100 h-100 bg-white p-3 rounded-top" style="z-index:100; overflow:auto; display:none">
    <a href="" id="backDetailBtn">
    <ion-icon class="d-inline-block align-middle" name="arrow-back"></ion-icon> 
    <span class="d-inline-block align-middle"> Kembali</span>
    </a>
    <div class="col-md-8 offset-md-2">
        @if(isset($pendaftaran))
        <h5>Data Santri</h5>
        
        <table class="table">
            <tbody>
                <tr>
                    <td scope="row" width="30%">Nama Lengkap</td>
                    <td width="5%">:</td>
                    <td>{{$pendaftaran->nama}}</td>
                </tr>
                {{-- <tr><td>No Tes</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->no_tes}} </td>
                </tr>
                <tr><td>No Ruang</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->no_ruang}} </td>
                </tr>
                <tr><td>NISN</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->nisn}} </td>
                </tr>
                <tr><td>Tahun Lulus</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->tahun_lulus}} </td>
                </tr> --}}
                <tr><td>Tempat & Tanggal Lahir</td>
                    <td>:</td>
                    <td>{{$pendaftaran->tempat_lahir}} , {{$pendaftaran->tanggal_lahir}} </td>
                </tr>
                <tr><td>Jenis Kelamin</td>
                    <td>:</td>
                    <td>{{$pendaftaran->kelamin}} </td>
                </tr>
                <tr><td>Anak ke</td>
                    <td>:</td>
                    <td>{{$pendaftaran->anakke}} dari {{$pendaftaran->anakdari}} bersaudara</td>
                </tr>
                {{-- <tr><td>Nama Sekolah Asal</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->sekolah_asal}} </td>
                </tr>
                <tr><td>Alamat Sekolah Asal</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->alamat_sekolah_asal}} </td>
                </tr>
                <tr><td>Kategori Sekolah Asal</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->kategori_sekolah_asal}}</td>
                </tr>
                <tr><td>Rencana tinggal saat belajar</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->rencana_tinggal}} </td>
                </tr> --}}
                {{-- <tr><td>Dorongan masuk pesantren</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->dorongan_masuk}} </td>
                </tr>
                <tr><td>Sumber informasi PSB</td>
                    <td>:</td>
                    <td>{{$dataCalonSantri->sumber_informasi}} </td>
                </tr> --}}
            </tbody>
        </table>
        <br>
        <h5>Data Orangtua Santri</h5>
        <table class="table">
            <tbody>
                <tr>
                    <td scope="row" width="30%">Nama Ayah</td>
                    <td width="5%">:</td>
                    <td>{{$pendaftaran->namaortu1}}</td>
                </tr>
                <tr><td>Nama Ibu/td>
                    <td>:</td>
                    <td>{{$pendaftaran->namaortu2}} </td>
                </tr>
                <tr><td>Nama Wali</td>
                    <td>:</td>
                    <td>{{$pendaftaran->namawali}} </td>
                </tr>
                
                <tr><td>No HP 1</td>
                    <td>:</td>
                    <td>{{$pendaftaran->tlp}} </td>
                </tr>
                <tr><td>No HP 2</td>
                    <td>:</td>
                    <td>{{$pendaftaran->tlp2}} </td>
                </tr>
              
            </tbody>
        </table>
        <br>
       
        <br><br><br>
        @endif
    </div>
</div>