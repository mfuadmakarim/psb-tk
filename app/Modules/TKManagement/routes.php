<?php
Route::group(['namespace' => 'App\Modules\TKManagement\Controllers', 'middleware' => 'web'], function() {

    //Register

    Route::get('/daftar', 'TKRegisterController@getRegister');
    Route::get('/daftar-asatidz', 'TKRegisterController@getRegisterAsatidz');
   
    Route::post('/daftar', 'TKRegisterController@postRegister');
    Route::post('/daftar-asatidz', 'TKRegisterController@postRegisterAsatidz');

    Route::get('/daftar-testing', 'TKRegisterController@getRegisterTesting');
    Route::post('/daftar-testing', 'TKRegisterController@postRegisterTesting');
    //check is phone exist
    Route::get('/daftar/check-phone-number', 'TKRegisterController@checkIsPhoneExist');

    //check is name exist
    Route::get('/daftar/check-name', 'TKRegisterController@checkIsNameExist');

    //check is date exist
    Route::get('/daftar/check-date', 'TKRegisterController@checkIsDateExist');

    //check age
    Route::get('/daftar/check-age', 'TKRegisterController@checkAge');

    //Register success
    Route::get('/daftar/success/{id}', 'TKRegisterController@registerSuccess');


    Route::get('/daftar/failed/{id}', 'TKRegisterController@registerFailed');

 });