@extends('Layouts::layout')
@section('content')
<div class="container" style="padding-top: 15px; padding-bottom: 100px;">
	<div class="col-md-6 offset-md-3">
		<div class="row" style="margin-bottom: 10px">
			<div class="col-md-12 text-center">
					<img width="100" src="{{asset('public/images/persistarogonglogo.png')}}">
					<h3>Pendaftaran Santri Baru TK Persis Tarogong</h3>
			</div>
		</div>
		<div class="row">
			<div class="alert alert-success text-center" style="width:100%">
				<!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
				<h4>Selamat!</h4>
				<h2><strong>{{$name}}</strong></h2><br>
				<h4>Telah terdaftar dengan nomor pendaftaran</h4>
				<h2><strong>PSBTK-Um-{{$no}}</strong></h2>

			</div>
            <div class="col-md-12 text-center"><a href="{{url('/')}}" title="Kembali"><button type="submit" class="btn btn-success" >Kembali</button></a></div>
		</div>
	</div>
</div>
@endsection