<?php

namespace App\Modules\User\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
use App\Modules\TKManagement\Models\User;
use App\Modules\TKManagement\Models\UserTesting;
use App\Modules\TKManagement\Models\RegisterGelombang;
use App\Modules\TKManagement\Models\RegisterSetup;
use Carbon\Carbon;
use DateTime;
class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $pendaftarMln = new PendaftarMlnModel;
        $userId = Auth::id();
        $pendaftaran = (new User)->where('user_id', $userId)->first();

        $status = $this->checkRegister();

        if($status == 'belum-buka'){
            return redirect('/countdown');
        }else if($status == 'buka'){
            return redirect('/registrasi');
        }else if($status == 'tutup'){
            return redirect('/closed');
        }
    }
    public function checkRegister(){
        $datenow = new DateTime('now');
        $setup = (new RegisterSetup)->getSetup();
        $datestart = new DateTime($setup->tgl_pendaftaran_start);
        $dateend = new DateTime($setup->tgl_pendaftaran_end);
        // dd($datenow, $datestart, $dateend);
        if($datenow < $datestart){
            return $status = 'belum-buka';
        }elseif($datenow >= $datestart && $datenow <= $dateend) {
            return $status = 'buka';
        }elseif($datenow > $dateend){
            return $status = 'tutup';
        }
    }
    public function registrasi(){
        $datenow = (new DateTime('now'))->format('Y-m-d');
        $gelombang = (new RegisterGelombang)->where('periode', $datenow)->first();

        $userId = Auth::id();
        $pendaftaran = (new User)->where('user_id', $userId)->first();
        // dd($gelombang);
        $sesi1_start = (new DateTime($gelombang->sesi1_start))->format('H:i:s');
        $sesi1_end = (new DateTime($gelombang->sesi1_end))->format('H:i:s');
        $kuota_sesi1 = $gelombang->kuota_sesi1;
        $sesi2_start = (new DateTime($gelombang->sesi2_start))->format('H:i:s');
        $sesi2_end = (new DateTime($gelombang->sesi2_end))->format('H:i:s');
        $kuota_sesi2 = $gelombang->kuota_sesi1;
        $kelas = $gelombang->kelas;
        $periode = $gelombang->periode;

        $timenow = (new DateTime('now'))->format('H:i:s');

        // dd($timenow, $sesi1_start, $sesi1_end, $sesi2_start, $sesi2_end);
        if($timenow < $sesi1_start){
            // dd('menunggu sesi 1');
            $statusSesi = 1;
            return view('TKManagement::register.infoPage', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas
            ));
        }elseif($timenow >= $sesi1_start && $timenow <= $sesi1_end){
            // dd('sesi 1 mulai');
            $statusSesi = 2;
            return view('User::DashboardView', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas,
                'periode' => $periode,
                'pendaftaran' => $pendaftaran,
                'code' => 'sesi1'
            ));
        }elseif($timenow > $sesi1_end && $timenow < $sesi2_start){
            // dd('sesi 1 selesai, menunggu sesi 2');
            $statusSesi = 3;
            return view('TKManagement::register.infoPage', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas
            ));
        }elseif($timenow >= $sesi2_start && $timenow <= $sesi2_end){
            // dd('sesi 2 mulai');
            $statusSesi = 4;
            return view('User::DashboardView', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas,
                'periode' => $periode
            ));
        }elseif($timenow > $sesi2_end){
            // dd('sesi 2 selesai');
            $statusSesi = 5;
            return view('TKManagement::register.infoPage', array(
                'statusSesi' => $statusSesi,
                'kelas' => $kelas
            ));
        }
    }
    public function countdown(){
        $setup = (new RegisterSetup)->getSetup();
        $datestart = $setup->tgl_pendaftaran_start;
        return view('TKManagement::register.countdownPage', array(
            'datestart' => $datestart
        ));
    }
    public function closed(){
        return view('TKManagement::register.closePage');
    }

    public function getRegister() {
        // (new SmsGatewayController)->sendRegisterSuccessConfirmation("085920691077", 'Testing SMS');
        // dd('register');
        $title = "Pendaftaran Santri Baru TK Persis Tarogong";
        $gelombang = new RegisterGelombang;
        $setup = (new RegisterSetup)->getSetup();

        $datenow = new DateTime('now'); 
        // dd($datenow);
        $datestart = new DateTime($setup->tgl_pendaftaran_start);
        $datestart->setTime(02,00);

        
        $dateend = new DateTime($setup->tgl_pendaftaran_end);
        $dateend->setTime(12,00);

        $gelombangbydate = $gelombang->getGelombangByDate($datenow->format('Y-m-d'));


        if($datenow >= $datestart && $datenow <= $dateend && $gelombangbydate){
            $kelas = $gelombangbydate->kelas;
            return array(
                'title' => $title,
                'kelas' => $kelas
            );
        }
        else{
            if($datenow > $dateend){
                $status = 99; //ditutup
            }
            elseif($datenow < $datestart){
                $status = 98; //belum dibuka
            }
            return array(
                'datenow' => $datenow->format('Y-m-d'),
                'datestart' => $datestart->format('Y-m-d'),
                'dateend' => $dateend->format('Y-m-d'),
                'title' => $title,
                'status' => $status,
            );            
        }
    }
    public function getProvinces(){
        return Facade::allProvinces();
    }
    public function getCities($provinceId){
        $data = Facade::findProvince($provinceId, 'cities');
        $cities = $data->cities;
        return $cities;
    }
    public function getDistricts($cityId){
        $data = Facade::findCity($cityId, 'districts');
        $districts = $data->districts;
        return $districts;
    }
    public function getVillages($districtId){
        $data = Facade::findDistrict($districtId, 'villages'); 
        $villages = $data->villages;
        return $villages;
    }

    public function cetakKartu($id){
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('id', $id)->first();
        $date = date('d/m/Y');
        return view('Registrasi::Mln.KartuPesertaView', array(
            'dataCalonSantri' => $dataCalonSantri,
            'date' => $date
        ));
    }

    public function cetakKwitansi($id){
        $pendaftarMln = new PendaftarMlnModel;
        $dataCalonSantri = $pendaftarMln->where('id', $id)->first();
        $dt = new DateTime($dataCalonSantri->created_at);

        $date = $dt->format('d/m/Y');
        // $created = strtotime()
        // $date = strtotime('d/m/Y', $dataCalonSantri->created_at);
        return view('Registrasi::Mln.KwitansiPesertaView', array(
            'dataCalonSantri' => $dataCalonSantri,
            'date' => $date
        ));
    }

    // public function getProvinces() {
    //     $district = Facade::allProvinces();
    //     return $district;
    // }

    // public function getDistrict($id) {
    //     $district = Facade::findProvince($provinceId, 'cities');
    //     return json_encode($district);
    // }

    // public function getSubDistrict($id) {
    //     $sub_district = Facade::findCity($cityId, 'districts');
    //     return json_encode($sub_district);
    // }
}