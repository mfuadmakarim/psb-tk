
    <br>
    <h5>Ubah Password</h5>
    <hr>
    <div class="row">
    	<form action="{{url('/registrasi/mln/create')}}" method="POST" role="form" enctype="multipart/form-data" class="col-md-8">
            {{ csrf_field() }}
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Password Lama</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_lengkap" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Password Baru</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_lengkap" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label col-form-label-sm">Konfirmasi Password</label>
                <div class="col-md-9">
                    <input 
                        type="text" 
                        name="nama_lengkap" 
                        class="form-control form-control-sm" 
                        placeholder="Nama lengkap" 
                        required>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </form>
    </div>