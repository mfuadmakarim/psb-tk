<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserModel extends Model {
	protected $table = 'users';

    public function __construct() {
        $this->table = 'users';
    }
}