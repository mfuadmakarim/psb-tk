@extends('Layouts::adminLayout')
@section('content')
<div class="container-fluid pl-5 pr-5 pb-5">
	<!-- <div class="row">
		<div class="col-md-6">
			<h4>Dashboard PSB TK Persis</h4>
		</div>
		<div class="col-md-6 text-right">
			Mode Testing:
			<input id="testToggle" type="checkbox" checked="" data-toggle="toggle">
		</div>
	</div> -->
	<div class="row" style="margin-top: 40px">
		<div class="col-md-4">
			<div class="card card-default">
				<div class="card-header">
					<h5 class="mb-0">Statistik Pendaftar</h5>
				</div>
				<div class="card-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Kelas</th>
									<th>Status</th>
									<th>Jumlah</th>
									<th></th>
								</tr>
							</thead>
                            <tbody>
                                @if(isset($dataPendaftarA))
                                    @foreach ($dataPendaftarA as $data)
                                        <tr>
										    <td>A</td>
                                            <td>{{$data->status}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
								@endif
								@if(isset($dataPendaftarB))
                                    @foreach ($dataPendaftarB as $data)
                                        <tr>
										    <td>B</td>
                                            <td>{{$data->status}}</td>
                                            <td>{{$data->total}}</td>
                                            <td>{{number_format($data->total/$total*100, 1)}}%</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td>Total</td>
                                        <td>{{$total}}</td>
                                        <td>100%</td>
                                    </tr>
                                @endif
                            </tbody>
{{--
							<tbody>
								@foreach($setup as $index => $data)
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								@endforeach
							</tbody>
--}}
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card card-default">
				<div class="card-header">
					<h5 class="mb-0">Setup Pendaftaran</h5>
				</div>
				<div class="card-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Tgl Mulai Daftar</th>
									<th>Tgl Akhir Daftar</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($setup as $index => $data)
								<tr>
									<td>{{$data->tgl_pendaftaran_start}}</td>
									<td>{{$data->tgl_pendaftaran_end}}</td>
									<td><a class="btn btn-primary btn-sm" 
										data-id="{{$data->id}}" 
										data-periode="{{$data->tgl_periode}}"
										data-start="{{$data->tgl_pendaftaran_start}}"
										data-end="{{$data->tgl_pendaftaran_end}}"
										data-toggle="modal" href='#edit-setup'>Edit</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="card card-default">
				<div class="card-header">
					<h5 class="mb-0">Gelombang</h5>
				</div>
				<div class="card-body">
					<table class="table table-hover" cellspacing="0">
						<thead>
							<tr>
								<th>Kelas</th>
								<th>Tanggal Pendaftaran</th>
								<th>Sesi 1 (Kuota)</th>
								<th>Sesi 2 (Kuota)</th>
								{{-- <th>Kuota</th> --}}
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($gelombang as $index => $data)
							<tr>
								<td>{{$data->kelas}}</td>
								<td>{{$data->periode}}</td>
								<td>{{Carbon\Carbon::parse($data->sesi1_start)->format('H:i')}} s/d 
									{{Carbon\Carbon::parse($data->sesi1_end)->format('H:i')}} 
									({{$data->kuota_sesi1}})
								</td>
								<td>{{Carbon\Carbon::parse($data->sesi2_start)->format('H:i')}} s/d 
									{{Carbon\Carbon::parse($data->sesi2_end)->format('H:i')}} 
									({{$data->kuota_sesi2}})</td>
								{{-- <td>{{$data->kuota}}</td> --}}
								<td><a class="btn btn-primary btn-sm edit-gelombang"
									data-id="{{$data->id}}"
									data-kelas="{{$data->kelas}}"
									data-periode1="{{$data->periode1}}"
									data-kuota="{{$data->kuota}}"
									data-toggle="modal" href='#edit-gelombang'>Edit</a></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-12">
			<div class="card card-info">
				<div class="card-header">
					<h5 class="mb-0">Record Pendaftar</h5>
				</div>
				<div class="card-body" style="overflow-x: scroll;">
					<!-- <form action="{{url('/delete')}}" method="POST" role="form">
						{{ csrf_field() }}
						<input type="checkbox" name="data" value="194">
						<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
					</form> -->
					<form action="{{url('/delete')}}" method="POST" role="form">
				    {{ csrf_field() }}
				    <input type="hidden" name="_method" value="delete">
					<table id="tableUserA" class="table table-hover datatable-show-all">
						<thead>
							<tr>
								<th></th>
								<th>No</th>
								<th>Kelas</th>
								<th>Kategori</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tempat Lahir</th>
								<th>Tanggal Lahir</th>
								<th>Anak ke</th>
								<th>Dari bersaudara</th>
								<th>Nama Ayah</th>
								<th>Nama Ibu</th>
								<th>Nama Wali</th>
								<th>Telp/HP 1</th>
								<th>Telp/HP 2</th>
								<th>Waktu Daftar</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody style="position: relative;">
							
							@foreach($user as $index => $data)
							<tr>
								<td><input type="checkbox" name="data[]" value="{{$data->id}}"></td>
								<td>{{$data->id}}</td>
								<td>{{$data->kelas}}</td>
								<td>{{$data->kategori}}</td>
								<td>{{$data->nama}}</td>
								<td>{{$data->kelamin}}</td>
								<td>{{$data->tempat_lahir}}</td>
								<td>{{$data->tanggal_lahir}}</td>
								<td>{{$data->anakke}}</td>
								<td>{{$data->anakdari}}</td>
								<td>{{$data->namaortu1}}</td>
								<td>{{$data->namaortu2}}</td>
								<td>{{$data->namawali}}</td>
								<td>{{$data->tlp}}</td>
								<td>{{$data->tlp2}}</td>
								<td>{{$data->created_at}}</td>
								<td>{{$data->status}}</td>
							</tr>
							@endforeach
							<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
							</form>

						</tbody>
					</table>
				</div>
			</div>
				
		</div>
	</div>
</div>
<div class="modal fade" id="edit-gelombang">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header d-table">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h5 class="mb-0">Edit Gelombang</h5>
			</div>
			<form action="{{url('/edit-gelombang')}}" method="POST" role="form">
			<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" name="id" id="id2-input" class="form-control" value="">
					<div class="form-group">
						<label for="">Kelas</label>
						<input type="text" name="kelas" class="form-control" id="kelas-input" value="">
					</div>
					<div class="form-group">
						<label for="">Tanggal Pendaftaran</label>
						<input type="date" name="periode" class="form-control" id="periode-input" value="">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Sesi 1 Mulai</label>
								<input type="time" name="sesi1_start" class="form-control" id="sesi1_start-input" value="">
							</div>
							<div class="form-group">
								<label for="">Sesi 1 Akhir</label>
								<input type="time" name="sesi1_end" class="form-control" id="sesi1_end-input" value="">
							</div>
							<div class="form-group">
								<label for="">Kuota Sesi 1</label>
								<input type="number" name="kuota_sesi1" class="form-control" id="kuota_sesi1-input" value="">
							</div>
						</div>
						<div class="col-md-6">
								<div class="form-group">
										<label for="">Sesi 2 Mulai</label>
										<input type="time" name="sesi2_start" class="form-control" id="sesi2_start-input" value="">
									</div>
									<div class="form-group">
										<label for="">Sesi 2 Akhir</label>
										<input type="time" name="sesi2_end" class="form-control" id="sesi2_end-input" value="">
									</div>
									<div class="form-group">
										<label for="">Kuota Sesi 2</label>
										<input type="number" name="kuota_sesi2" class="form-control" id="kuota_sesi2-input" value="">
									</div>
						</div>
					</div>
					
					
				
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="edit-setup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header d-table">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h5 class="mb-0">Edit Setup</h5>
			</div>
			<form action="{{url('/edit-setup')}}" method="POST" role="form">
			<div class="modal-body">
					{{ csrf_field() }}
					<input type="hidden" name="id" id="id-input" class="form-control" value="">
					<div class="form-group">
						<label for="">Periode Usia</label>
						<input type="date" name="tgl_periode" class="form-control" id="periode-input" value="">
					</div>
					<div class="form-group">
						<label for="">Tanggal Pendaftaran Mulai</label>
						<input type="date" name="tgl_pendaftaran_start" class="form-control" id="start-input" value="">
					</div>
					<div class="form-group">
						<label for="">Tanggal Pendaftaran Akhir</label>
						<input type="date" name="tgl_pendaftaran_end" class="form-control" id="end-input" value="">
					</div>
					
				
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</form>
		</div>
	</div>
</div>


@endsection