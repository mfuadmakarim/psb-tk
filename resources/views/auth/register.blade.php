@extends('Layouts::layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 offset-md-4"><br><img src="{{asset('http://psb.persistarogong.com/wp-content/uploads/2018/11/weblogo-persista-2.png')}}" width="100%"/><br><br>
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center"><b>BUAT AKUN PSB TK</b></div>
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="role" value="tk"/>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-12 control-label">Username</label>

                            <div class="col-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-12 control-label">Alamat Email</label>

                            <div class="col-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                        <div class="form-group{{ $errors->has('no_hp') ? ' has-error' : '' }}">
                            <label for="no_wa" class="col-12 control-label">No HP/WhatsApp</label>

                            <div class="col-12">
                                <input id="no_wa" type="number" class="form-control" name="no_wa" value="{{ old('no_wa') }}" required>

                                @if ($errors->has('no_wa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_wa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
{{--
                        <div class="form-group{{ $errors->has('no_wa') ? ' has-error' : '' }}">
                            <label for="no_wa" class="col-12 control-label">No Whatsapp</label>

                            <div class="col-12">
                                <input id="no_wa" type="number" class="form-control" name="no_wa" value="{{ old('no_wa') }}">

                                @if ($errors->has('no_wa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('no_wa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
--}}
                        

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-12 control-label">Password</label>

                            <div class="col-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-12 control-label">Konfirmasi Password</label>

                            <div class="col-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-12">
                                <button type="submit" class="btn btn-danger btn-block">
                                    Buat Akun
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
