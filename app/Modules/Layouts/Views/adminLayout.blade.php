<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/jquery.dataTables.min.css')}}"/>
    <link rel=stylesheet href="{{asset('public/css/sweetalert.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type=text/javascript>var app_url={!!json_encode(url('/'))!!}</script>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

    <title>PSB Admin - TK Persis</title>
    <style>
        .dt-buttons{
            display:inline-block;
        }
    </style>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-danger">
  <a class="navbar-brand" href="#">PSB Admin - <b>TK Persis</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav ml-auto">
       @if (Auth::guest())
            <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Buat Akun</a></li>
        @else
            <li class="nav-item">
                <a href="#" class="nav-link">
                    {{ Auth::user()->name }}
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                            Logout
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
            </li> 
        @endif
    </ul>
  </div>
</nav>
    @yield('content')
    <script src="{{asset('public/js/sweetalert.min.js')}}"></script>
    <script>var fade_out=function(){$("#alert").fadeOut();}
setTimeout(fade_out,5000);</script>
@include('sweet::alert')
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('public/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('public/js/popper.min.js')}}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
    <script>
        $('.to-santri-panel').click(function(e){
            e.preventDefault();
            $('a[href="#santri-panel"]').tab('show');
        });
        $('.to-ortu-panel').click(function(e){
            e.preventDefault();
            $('a[href="#ortu-panel"]').tab('show');
        });
        $('.to-alamat-panel').click(function(e){
            e.preventDefault();
            $('a[href="#alamat-panel"]').tab('show');
        });
        $('.to-jurusan-panel').click(function(e){
            e.preventDefault();
            $('a[href="#jurusan-panel"]').tab('show');
        });
        $('.to-pembayaran-panel').click(function(e){
            e.preventDefault();
            $('a[href="#pembayaran-panel"]').tab('show');
        });
        $('#backDetailBtn').click(function(e){
            e.preventDefault();
            $("#detail").css("display", "none");
        });
        $('.show-detail-btn').click(function(e){
            e.preventDefault();
            $("#detail").css("display", "block");
        });
        $('body').click(function(event){
        // check if the clicked element is a descendent of navigation 
        if ($(event.target).closest('#optionSantri').length) {
            return; //do nothing if event target is within the navigation
        } else {
            $('#optionSantri').removeClass('show');
        // do something if the event target is outside the navigation
            // code for collapsing menu here...
        }
        });
    </script>
    <script src="{{asset('public/js/address.js') }}"></script>
    <script src="{{asset('public/js/jurusan.js') }}"></script>
    <script src="{{asset('public/js/datasantri.js') }}"></script>
    
    <script src="{{asset('public/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('public/js/pdfmake.min.js')}}"></script>
    <script src="{{asset('public/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('public/js/buttons.html5.min.js')}}"></script>
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tableUserA').DataTable({
                "paging": true,
                "sorting": true,
                "order":[],
                dom: 'Bfrtip',
                responsive: true,
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
            });
            var filterA = $('#tableUserA').DataTable();
            filterA.column( 2 ).data().filter( function ( value, index ) {
                return value == 'A';
            } );
        } );
    </script>
    <script>
        $(document).ready( function () {
            $(document).ready( function () {
                $('#listSantri').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ],
                    order: [[ 6, "desc" ]]
                });
                $('#listPembayaran').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ],
                    order: [[ 4, "desc" ]]
                });
                $('#listPembayaranOnline').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel',
                        'csv',
                        'pdf',
                        'print'
                    ],
                    order: [[ 7, "desc" ]]
                });
            } );
        } );    
    </script>
    <script type="text/javascript">
	$('#edit-setup').on('shown.bs.modal', function (e) {
  		var id =  $('a[href="#edit-setup"]').data("id");
  		var periode = $('a[href="#edit-setup"]').data("periode");
  		var start = $('a[href="#edit-setup"]').data("start");
  		var end = $('a[href="#edit-setup"]').data("end");

  		$("#id-input").val(id);
  		$("#periode-input").val(periode);
  		$("#start-input").val(start);
  		$("#end-input").val(end);
  		
	});
	$(".edit-gelombang").click(function(){
		var id =  $(this).data("id");
  		var kelas = $(this).data("kelas");
  		var periode1 = $(this).data("periode1");
  		var kuota = $(this).data("kuota");
  		$('#edit-gelombang').on('shown.bs.modal', function (e) {
  		

	  		$("#id2-input").val(id);
	  		$("#periode1-input").val(periode1);
	  		$("#kelas-input").val(kelas);
	  		$("#kuota-input").val(kuota);
	  		
		});
	});
	
</script>
<!-- <script type="text/javascript">
	$(document).ready(function () {
		testToggle();
	});
	function testToggle() {
		$('#testToggle').on('keyup change', function () {
			var value = $('#testToggle:checked').length;	
			if (value) {
				$.ajax({
					url: app_url + '/admin/test-toggle',
					type: "GET",
					dataType: "json",
					data: {value: value},
					error: function (req, textStatus, errorThrown) {
						//this is going to happen when you send something different from a 200 OK HTTP
						testToggle();
					},
					success: function (data) {
						if (data.status == 1) {
							
						} else {
							
						}
					},
				});
			};
		});
	};
</script> -->
<script src="{{asset('public/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('public/js/jszip.min.js')}}"></script>
<script src="{{asset('public/js/pdfmake.min.js')}}"></script>
<script src="{{asset('public/js/vfs_fonts.js')}}"></script>
<script src="{{asset('public/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('public/js/buttons.print.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap-toggle.min.js')}}"></script>
  </body>
</html>