<?php
// Route::group(['namespace' => 'App\Http\Controllers\Auth', 'middleware' => 'web'], function() {
// 	Route::get('/admin', 'LoginController');
// });
Route::group(['namespace' => 'App\Modules\AdminManagement\Controllers', 'middleware' => 'web'], function() {
	Route::get('/admin', 'DashboardController@getDashboard');
	Route::post('/edit-setup', 'DashboardController@updateSetup');
	Route::post('/edit-gelombang', 'DashboardController@updateGelombang');
    Route::delete('/delete', 'DashboardController@deleteUser');
      //check is name exist
    Route::get('/admin/test-toggle', 'DashboardController@testToggle');
    Route::get('/dashboard', 'DashboardController@dashboard');
});