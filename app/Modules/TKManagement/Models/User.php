<?php

namespace App\Modules\TKManagement\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class User extends Model {
    use SoftDeletes;

	protected $table = 'pendaftaran';
    // protected $dates = 'deleted_at';
    
    public function __construct() {
        $this->table = 'pendaftaran';
    }

    public function getUserById($id){
        $data = DB::table('pendaftaran')
                ->where('id', $id)
                ->first();
    }
    public function getUserByKelas(){
        $data = DB::table('pendaftaran')
                ->where('kelas', 'B');
    }
    public function isPhoneNumberExist($phone) {
        $data = DB::table('pendaftaran')
                ->where('tlp', '=', $phone)
                ->first();

        return $data;
    }

    public function isNameExist($name) {
        $data = DB::table('pendaftaran')
                ->where('nama', '=', $name)
                ->first();

        return $data;
    }

    public function isDateExist($date, $name) {
        $data = DB::table('pendaftaran')
                ->where('nama', '=', $name)
                ->where('tanggal_lahir', '=', $date)         
                ->first();

        return $data;
    }

    public function getCountKelasAStatus(){
        $data = DB::table('pendaftaran')
           ->select('status', DB::raw('count(*) as total'))
           ->where('kelas', '=', 'A')
           ->groupBy('status')
           ->get();
        return $data;
    }
    public function getCountKelasBStatus(){
        $data = DB::table('pendaftaran')
           ->select('status', DB::raw('count(*) as total'))
           ->where('kelas', '=', 'B')
           ->groupBy('status')
           ->get();
        return $data;
    }
}