@extends('CommonModule::layouts.layout')
@section('content')

<div class="container" style="padding-top: 15px; padding-bottom: 100px;">
	<div class="col-md-6 col-md-offset-3">
		<div class="row" style="margin-bottom: 10px">
			<div class="col-md-12 text-center">
					<img width="100" src="{{asset('public/images/logo.png')}}">
					<h3>Pendaftaran Santri Baru TK Persis Tarogong</h3>
			</div>
		</div>
		<div class="row">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h2 class="panel-title">Pernyataan Kesepakatan Orang Tua / Wali</h2>
				</div>
				<div class="panel-body">
					<ol>
						<li>Bersedia bekerjasama dengan pihak sekolah dan membina santri yang bersangkutan</li>
						<li>Mentaati peraturan yang ditetapkan sekolah</li>
						<li>Menyetujui dan bersedia membayar biaya pendidikan yang telah ditetapkan</li>
						<li>Bersedia membayar SPP dan uang makan sebanyak 12 kali dalam setahun</li>
					</ol>				
				</div>
			</div>
		</div>
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Kelas Pendaftaran</h3>
				</div>
				<div class="panel-body">
					<h4>Kelas {{$kelas}} / Kelompok <span id="kelas">{{$kelas}}</span></h4>
					@if($kelas=='A')
						<p>Berusia Minimal 4 Tahun (Pada bulan Juni 2018)</p>
					@else
						<p>Berusia Minima 5 Tahun (Pada bulan Juni 2018)</p>
					@endif
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-md-6 col-md-offset-3">
		<div class="row">
			<form action="{{url('/daftar-asatidz')}}" method="POST" class="form-horizontal" role="form">
			{{ csrf_field() }}
			<input type="hidden" name="class" class="form-control" value="{{$kelas}}">
			<input type="hidden" name="kategori" value="Asatidz">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Identitas Anak</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12">
								<div class="form-group">
									<label>Nama Lengkap (Sesuai Akte Lahir) <b style="color: red">*</b></label>
									<input type="text" placeholder="Nama Lengkap" name="name" id="name" class="form-control" value="{{old('name')}}" required="required" title="Nama Lengkap">
									@if ($errors->all())
										<div class="alert alert-warning">
											<strong>{{ $errors->first('name') }}</strong>
										</div>
									@endif
									<div id='name-validation'>
									</div>
								</div>
								
								<div class="form-group">
									<label>Jenis Kelamin<b style="color: red">*</b></label>
									<div class="radio" required="required">
										<label>
											<input type="radio" name="gender" value="L">
											Laki-laki
										</label>
										<label>
											<input type="radio" name="gender" value="P">
											Perempuan
										</label>
									</div>
								</div>
								<div class="form-group">
									<label>Tempat Lahir<b style="color: red">*</b></label>
									<input type="text" name="birth_place" class="form-control" value="{{old('birth_place')}}" required="required" title="Tempat Lahir">
								</div>
								<div class="form-group">
									<label>Tanggal Lahir <b style="color: red">*</b></label>
									<div class="row">
										<div class="col-md-4">
											<input type="number" min="0" max="31" name="tanggal" id="tanggal" class="form-control" value="{{old('date')}}" required="required" placeholder="Tanggal" title="">
										</div>
										<div class="col-md-4">
											<select name="bulan" id="bulan" class="form-control" required="required">
												<option value="">Bulan</option>
												<option value="01">Januari</option>
												<option value="02">Februari</option>
												<option value="03">Maret</option>
												<option value="04">April</option>
												<option value="05">Mei</option>
												<option value="06">Juni</option>
												<option value="07">Juli</option>
												<option value="08">Agustus</option>
												<option value="09">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
										</div>
										<div class="col-md-4">
											<input type="number" min="2011" max="2018" name="tahun" id="tahun" class="form-control" value="{{old('year')}}" required="required" placeholder="Tahun" title="">
										</div>
									</div>
									<!-- <input type="date" name="birth_date" id="date" class="form-control" value="{{old('birth_date')}}" required="required" title="Tanggal Lahir"> -->
									@if ($errors->all())
										<div class="alert alert-warning">
											<strong>{{ $errors->first('date') }}</strong>
										</div>
									@endif
									<div id='date-validation'>
									</div>
									<div id='age-validation'>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-xs-6">
											<label>Anak Ke-</label>
											<input type="number" name="anakke" class="form-control" value="{{old('anakke')}}" required="required" pattern="" title="">
										</div>
										<div class="col-xs-6">
											<label>Dari - Bersaudara</label>
											<input type="number" name="anakdari" class="form-control" value="{{old('anakdari')}}" required="required" pattern="" title="">
										</div>
									</div>
								</div>
						
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Identitas Orang Tua</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12">
							<div class="form-group">

								<label>Nama Ayah Kandung:<b style="color: red">*</b></label>
									<input type="text" value="{{old('namaortu1')}}" name="namaortu1" class="form-control" id="penyakit" placeholder="Nama Ayah Kandung" required>
							</div>


							<div class="form-group">

								<label>Nama Ibu Kandung:<b style="color: red">*</b></label>
									<input type="text" value="{{old('namaortu2')}}" name="namaortu2" class="form-control" placeholder="Nama Ibu Kandung" id="penyakit" required>
							</div>


							<div class="form-group">

								<label>Nama Wali <i>(jika ada)</i>:</label>
									<input type="text" name="namawali" value="{{old('namawali')}}" class="form-control" id="penyakit">
							</div>


							<div class="form-group">

								<label>Alamat Lengkap :<b style="color: red">*</b></label>
									<textarea name="alamat" class="form-control" rows="5" id="comment" placeholder="Alamat lengkap" required>{{old('alamat')}}</textarea>
							</div>
							<div class="form-group">
									<label>No. HP (1):<b style="color: red">*</b></label>
									<input type="text" placeholder="Nomor HP" name="phone_number" id="phoneNumber" class="form-control" value="{{old('phone_number')}}" required="required" title="Nomor HP">
									<p style="margin-top: 5px; font-style: italic;">Kami akan mengirimkan konfirmasi jika pendaftaran berhasil</p>
									@if ($errors->all())
										<div class="alert alert-warning">
											<strong>{{ $errors->first('phone_number') }}</strong>
										</div>
									@endif
									<div id='phone-validation'>
									</div>
							</div>

							<div class="form-group">

								<label>No. Tlp/HP (2):</label>
									<input type="text" name="tlp2" class="form-control" value="{{old('tlp2')}}" id="penyakit">
							</div>

							<div class="form-group">

								<label>No. Tlp/HP (3):</label>
									<input type="text" name="tlp3" class="form-control" value="{{old('tlp3')}}" id="penyakit">
							</div>
							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="" name="chk" id="check">
										Data yang saya masukan di atas sudah benar (Centang)
									</label>
								</div>
							</div>

						</div>
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="form-group">
						
							<button type="submit" id="btnD" class="btn btn-success btn-block">Submit</button>
						
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="{{asset('public/js/input-validation.js?v=5')}}"></script>

@endsection