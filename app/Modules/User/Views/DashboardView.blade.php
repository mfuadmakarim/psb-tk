@extends('Layouts::layout')
@section('content')
@include('User::DetailView')
<div class="container-fluid">
<nav class="mt-3">
    <div class="nav nav-pills nav-fill" id="nav-tab" role="tablist">
        <a class="nav-item nav-link active" 
            id="nav-staus-tab" 
            data-toggle="tab" 
            href="#status-panel" 
            role="tab" 
            aria-controls="nav-status" 
            aria-selected="true">
            Status Pendaftaran
        </a>
        <a class="nav-item nav-link {{isset($pendaftaran) ? 'disabled' : ''}}" 
            id="nav-santri-tab" 
            data-toggle="tab" 
            href="#santri-panel" 
            role="tab" 
            aria-controls="nav-santri" 
            aria-selected="false">
            Formulir Pendaftaran<br>
            <span class="small text-success">
                {{isset($pendaftaran) ? "Anda sudah mengisi pendaftaran" : ""}}
            </span>
        </a>
        
    </div>
</nav>
</div>
    <hr>
<div class="container">      
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="status-panel" role="tabpanel" aria-labelledby="nav-status-tab">
            <br>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-md-4">
                    <div class="card 
                    {{!isset($pendaftaran) ? 'bg-light' : ''}}
                    {{isset($pendaftaran) && $pendaftaran->status == 'Berhasil' ? 'bg-success text-white' : ''}}
                    {{isset($pendaftaran) && $pendaftaran->status == 'Pending' ? 'bg-warning text-white' : ''}} mb-3">
                    <div class="card-header">Status Pendaftaran 
                        <span class="float-right">
                            <b>
                                {{!isset($pendaftaran) ? 'BELUM DAFTAR' : ''}}
                                {{isset($pendaftaran) && $pendaftaran->status == 'Berhasil' ? 'BERHASIL' : ''}}
                                {{isset($pendaftaran) && $pendaftaran->status == 'Pending' ? 'PENDING' : ''}}
                            </b>
                        </span></div>
                        <div class="card-body">
                            <div>
                            @if(isset($pendaftaran)) 
                                @if($pendaftaran->status == 'Berhasil')
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><b>{{!isset($pendaftaran) ? '-' : $pendaftaran->nama}}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Kelas</td>
                                            <td>:</td>
                                            <td><b>{{!isset($pendaftaran) ? '-' : $pendaftaran->kelas}}</b></td>
                                        </tr>
                                        <tr>
                                            <td>No Pendaftaran</td>
                                            <td>:</td>
                                            <td><b>PSBTK-Um-{{!isset($pendaftaran) ? '-' : $pendaftaran->id}}</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <span class="small"><a href="" class="show-detail-btn btn btn-sm btn-light mt-3 btn-block">Lihat Detail</a> 
                                    {{-- <a  href="#" class="to-santri-panel">{{$noTes != null ? '' : '| Edit'}}</a> --}}
                                </span> 
                                @elseif($pendaftaran->status == 'Pending')
                                <b>Mohon maaf Pendaftaran Gagal</b><br><br>
                                Atas Nama: <b>{{!isset($pendaftaran) ? '-' : $pendaftaran->nama}}</b>
                                <br><br>
                                Jumlah pendaftar telah mencapai kuota
                                @endif
                            @else
                                Silakan <a  href="#" class="to-santri-panel"><b>Isi Formulir Pendaftaran</b></a>
                                <br><br>
                                Sebelum melakukan pendaftaran, harap membaca <b>Pernyataan Kesepakatan</b> dan <b>Panduan</b> dengan seksama terlebih dahulu
                            @endif
                            </div>
                        </div>
                    <div class="card-footer">
                            @if(!isset($pendaftaran))
                                <i>Belum mengisi Formulir Pendaftaran</i>
                            @else
                                @if($pendaftaran->status == 'Berhasil')
                                <i>Download Kartu Tanda Pendaftaran</i>
                                <a href="{{url('cetak-kartu/'.$pendaftaran->id)}}" class="btn btn-sm btn-light btn-block">Download Kartu Tanda Pendaftaran</a>
                                @elseif($pendaftaran->status == 'Pending')
                                <i>Terima kasih atas kepercayaan anda.</i>
                                @endif
                            @endif
                    </div>
                    </div>
                </div>

                <div class="col-md-8">
			<div class="card mb-3" style="width:100%">
				<div class="card-header">
					@if(isset($kelas))
					<h5 class="card-title">Pendaftaran Kelas TK {{$kelas}}</h5>
					@endif
				</div>
				<div class="card-body">
					@if(isset($kelas))
						@if($kelas=='A')
							<ul>
							    <li>Berusia minimal <b>4 tahun</b> pada bulan <b>Juni 2019</b></li>
							    <li>Tanggal kelahiran <b>tidak lebih dari 30 Juni 2015</b></li>
							</ul>
						@else
							<ul>
							    <li>Berusia minimal <b>5 tahun</b> pada bulan <b>Juni 2019</b></li>
							    <li>Tanggal kelahiran <b>tidak lebih dari 30 Juni 2014</b></li>
							</ul>
						@endif
					@endif
				</div>
			</div>
        			<div class="card mb-3">
        				<div class="card-header">
        					<h5 class="card-title">Pernyataan Kesepakatan Orang Tua / Wali</h5>
        				</div>
        				<div class="card-body">
        					<ol>
        						<li>Bersedia bekerjasama dengan pihak sekolah dan membina santri yang bersangkutan</li>
        						<li>Mentaati peraturan yang ditetapkan sekolah</li>
        						<li>Menyetujui dan bersedia membayar biaya pendidikan yang telah ditetapkan</li>
        						<li>Bersedia membayar SPP dan uang makan sebanyak 12 kali dalam setahun</li>
        					</ol>				
        				</div>
        			</div>

        			<div class="card mb-3">
        				<div class="card-header">
        					<h5 class="card-title">Panduan Daftar Online</h5>
        				</div>
        				<div class="card-body">
        					<ol>
                             	<li><strong>Kunjungi website</strong> pendaftaran online
                                    <ul>
                                     	<li><em>Pendaftaran online dimulai tepat waktu sesuai jadwal yang ditetapkan:</em>
                                            <ul>
                                                <li>TK A: Jum'at, 1 Maret 2019 pukul 08.00 & 10.00 WIB</li>
                                             	<li>TK B: Sabtu, 2 Maret 2019 pukul 08.00 & 10.00 WIB</li>
                                            </ul>    
                                        </li>
                                    </ul>
                                </li>
                             	<li><strong>Buat akun</strong> pendaftaran baru, isi Username, no HP/WA, dan membuat password baru.
                                    <ul>
                                     	<li><em>1 No HP/WA hanya untuk 1 akun pendaftaran</em></li>
                                    </ul>
                                </li>
                             	<li><strong>Login</strong> menggunakan No HP &amp; password yang telah dibuat</li>
                             	<li><strong>Isi Formulir</strong> Pendaftaran Calon Santri Baru dengan lengkap dan benar
                                    <ul>
                                     	<li>Pengisian nama anak <strong>harus sesuai Akte Lahir</strong></em>
                                     	    <ul>
                                             	<li><em>jika nama anak tidak sesuai dengan Akte Lahir, maka pendaftaran bisa dinyatakan batal</em></li>
                                     	    </ul>
                                     	    </li>
                                     	<li>Pengisian tanggal lahir anak <strong>harus sesuai Akte Lahir</strong></em>
                                     	    <ul>
                                             	<li><em>pendaftaran tidak dapat diproses jika tanggal lahir tidak sesuai persyaratan</em></li>
                                             	<li><em>jika terbukti memanipulasi tanggal lahir, maka pendaftaran akan dinyatakan batal</em></li>
                                     	    </ul>
                                     	    </li>
                                    </ul>
                                </li>
                             	<li><strong>Submit data</strong> pendaftaran, jika data sudah diisi dengan lengkap.
                                    <ul>
                                     	<li><em>Jika pendaftaran berhasil, akan muncul keterangan <b>Pendaftaran Berhasil</b> beserta nomor pendaftaran</em></li>
                                     	<li><em>Jika pendaftaran tidak berhasil, akan muncul keterangan <b>Pendaftaran Gagal</b>, karena jumlah pendaftar telah mencapai daya tampung</em></li>
                                    </ul>
                                </li>
                            	<li><b>Download dan Print</b> Kartu Tanda Pendaftaran sebagai bukti pendaftaran telah berhasil</li>
                            	<li>Lakukan <strong>Daftar Ulang</strong> sesuai jadwal yang ditetapkan dengan menunjukkan Kartu Tanda Pendaftara dan membawa persyaratan administrasi dan membayar biaya pendidikan.</li>
                            </ol>				
        				</div>
        			</div>

                </div>
                

            </div>
        </div>
        <div class="tab-pane fade" id="santri-panel" role="tabpanel" aria-labelledby="nav-santri-tab">
            @include('TKManagement::register.registerPage')
        </div>
    </div>
</div>
 <br><br><br>
@endsection