<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel=icon type=image/png href="{{url('https://res.cloudinary.com/agromaret/image/upload/v1515575053/public/default/images/agrofav.png')}}"> -->
<link href="{{asset('public/fonts/font-googleapis.css')}}" rel=stylesheet>
<link href="{{asset('public/css/font-awesome.min.css')}}" rel=stylesheet>
<title>@if(isset($title))
	{{$title}}
	@endif
</title>
<link rel=stylesheet href="{{asset('public/css/bootstrap.min.css')}}">
<!-- <link rel=stylesheet href="{{asset('public/css/style.css')}}"> -->
<script type=text/javascript>var app_url={!!json_encode(url('/'))!!}</script>
<script src="{{asset('public/js/jquery-1.12.4.js')}}"></script>
<script src="{{asset('public/js/bootstrap.min.js')}}"></script>
<link rel=stylesheet href="{{asset('public/css/sweetalert.css')}}">
<script src="{{asset('public/js/sweetalert.min.js')}}"></script>

<link rel=stylesheet href="{{asset('public/css/jquery.dataTables.min.css')}}">
<link rel=stylesheet href="{{asset('public/css/buttons.dataTables.min.css')}}">

<link rel=stylesheet href="{{asset('public/css/bootstrap-toggle.min.css')}}">
<style type="text/css">
	body{
		font-family: 'Segoe UI';
	}
	.d-inline{
		display: inline-block;
		float: left;
	}
</style>
</head>
@yield('content')
<script>var fade_out=function(){$("#alert").fadeOut();}
setTimeout(fade_out,5000);</script>
@include('sweet::alert')
<script type="text/javascript">
	$(document).ready(function() {
	    $('#tableUserA').DataTable({
	    	"paging": true,
        	"sorting": true,
        	"order":[],
        	dom: 'Bfrtip',
        	responsive: true,
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	            'csvHtml5',
	            'pdfHtml5'
	        ]
	    });
	} );
</script>
</body>
</html>