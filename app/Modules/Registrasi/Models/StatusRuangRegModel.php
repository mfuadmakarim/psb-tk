<?php

namespace App\Modules\Registrasi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusRuangRegModel extends Model {
	protected $table = 'status_ruang_reg';

    public function __construct() {
        $this->table = 'status_ruang_reg';
    }
    public $timestamps = false;

    public function getCountCode($code){
        $countCode = DB::table('status_ruang_reg')
            ->where('code', $code)
            ->get()
            ->count();

        return $countCode;
    }
}