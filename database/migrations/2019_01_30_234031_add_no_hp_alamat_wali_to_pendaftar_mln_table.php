<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoHpAlamatWaliToPendaftarMlnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendaftar_mln', function($table) {
            $table->string('no_hp_wali')->nullable();
            $table->string('alamat_wali')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pendaftar_mln', function($table) {
            $table->dropColumn('no_hp_wali');
            $table->dropColumn('alamat_wali');
        });
    }
}
