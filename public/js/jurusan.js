$(document).ready(function() {
  $('select[name="pilihan_jurusan_1"]').on("change", function() {
    var state = $(this).val();
    $('select[name="pilihan_jurusan_2"] option[value="' + state + '"]').hide();
    $('select[name="pilihan_jurusan_3"] option[value="' + state + '"]').hide();
  });
  $('select[name="pilihan_jurusan_2"]').on("change", function() {
    var state = $(this).val();
    $('select[name="pilihan_jurusan_1"] option[value="' + state + '"]').hide();
    $('select[name="pilihan_jurusan_3"] option[value="' + state + '"]').hide();
  });
  $('select[name="pilihan_jurusan_3"]').on("change", function() {
    var state = $(this).val();
    $('select[name="pilihan_jurusan_1"] option[value="' + state + '"]').hide();
    $('select[name="pilihan_jurusan_2"] option[value="' + state + '"]').hide();
  });
});
