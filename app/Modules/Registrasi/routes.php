<?php
Route::group(['namespace' => 'App\Modules\Registrasi\Controllers', 'middleware' => 'web', 'prefix' => '/registrasi/mln'], function() {

    //Home
    Route::get('/', 'RegistrasiMlnController@index');
    Route::get('/add/new', 'RegistrasiMlnController@add');
    Route::post('/create', 'RegistrasiMlnController@create');
    Route::get('/edit/{id}', 'RegistrasiMlnController@edit');
    Route::post('/update', 'RegistrasiMlnController@update');
    Route::post('/update/orang-tua', 'RegistrasiMlnController@updateOrtu');
    Route::post('/update/jurusan', 'RegistrasiMlnController@updateJurusan');
    Route::delete('/delete/{id}', 'RegistrasiMlnController@delete');

    Route::post('/konfirmasi-pembayaran', 'RegistrasiMlnController@updatePembayaran');
 });