<?php

namespace App\Modules\User\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Laravolt\Indonesia\Facade;
use App\Http\Controllers\Controller;
use App\Modules\Registrasi\Models\PendaftarMlnModel;
use App\Modules\Registrasi\Models\PembayaranMlnModel;
use App\Modules\Registrasi\Models\StatusRegistrasiModel;
use App\Modules\TKManagement\Models\User;
use App\Modules\TKManagement\Models\UserTesting;
use App\Modules\TKManagement\Models\RegisterGelombang;
use App\Modules\TKManagement\Models\RegisterSetup;
use Carbon\Carbon;
use DateTime;
class CountdownController extends Controller
{
    public function index(){
        $setup = (new RegisterSetup)->getSetup();
        $datestart = $setup->tgl_pendaftaran_start;
        return view('TKManagement::register.countdownPage', array(
            'datestart' => $datestart
        ));
    }
}
