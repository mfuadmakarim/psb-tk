<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaranTsnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran_tsn', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->string('bank_pengirim')->nullable();
            $table->string('nama_pengirim')->nullable();
            $table->string('norek_pengirim')->nullable();
            $table->string('bank_penerima')->nullable();
            $table->string('jumlah_transfer')->nullable();
            $table->string('tanggal_transfer')->nullable();
            $table->string('status')->nullable();
            $table->string('validasi_oleh')->nullable();
            $table->string('tanggal_validasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran_tsn');
    }
}
