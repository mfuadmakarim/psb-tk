<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $no_wa = $request->input('no_wa');
        // $password =
        if (Auth::attempt(['no_wa' => $no_wa, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }
}
