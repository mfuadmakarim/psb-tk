<?php

namespace App\Modules\Registrasi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusRegistrasiModel extends Model {
	protected $table = 'status_registrasi';

    public function __construct() {
        $this->table = 'status_registrasi';
    }
    public $timestamps = false;
}