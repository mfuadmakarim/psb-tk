<?php

namespace App\Modules\Registrasi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusRuanganModel extends Model {
	protected $table = 'status_ruangan';

    public function __construct() {
        $this->table = 'status_ruangan';
    }
    public $timestamps = false;
}