@extends('Layouts::layout')
@section('content')
<div class="container" style="padding-top: 15px; padding-bottom: 100px;">
	<div class="col-md-6 offset-md-3">
		<div class="row" style="margin-bottom: 10px">
			<div class="col-md-12 text-center">
					<img width="100" src="{{asset('public/images/persistarogonglogo.png')}}">

					<h3>Pendaftaran Online Santri Baru <br>TK Persis Tarogong</h3>

			</div>
		</div>
		<div class="row">
			<div class="alert alert-info text-center" style="width:100%">
				<!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
				{{-- @if($status=='98')
					<h3 style="margin-top: 0;">Pendaftaran Belum Dibuka</h4>
				@elseif($status=='99')
					<h3 style="margin-top: 0;">Pendaftaran Ditutup</h4>
				@endif --}}


				
				@if(isset($allGelombang))
					@foreach($allGelombang as $data)
					    <h5>Waktu Pendaftaran TK {{$data->kelas}}: <br>{{Carbon\Carbon::parse($data->periode)->format('d-M-Y')}} </h5>
						<p>Pukul {{Carbon\Carbon::parse($data->sesi1_start)->format('H:i')}} 
							- {{Carbon\Carbon::parse($data->sesi1_end)->format('H:i')}} (sesi 1) 
							& {{Carbon\Carbon::parse($data->sesi2_start)->format('H:i')}} 
							- {{Carbon\Carbon::parse($data->sesi2_end)->format('H:i')}} (sesi 2)
						{{-- <br>TK B: pukul 14.00 - 14.40 (sesi 1) & 14.45 - 15.00 (sesi 2)</p> --}}
					@endforeach
				@endif
						
			</div>
		</div>
	</div>
</div>
@endsection