<?php

namespace App\Modules\Registrasi\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class PembayaranMlnModel extends Model {
	protected $table = 'pembayaran_mln';

    public function __construct() {
        $this->table = 'pembayaran_mln';
    }

    public function getPembayaran(){
        $pembayaran = DB::table('pembayaran_mln')
            ->join('pendaftar_mln', 'pembayaran_mln.pendaftar_id', '=', 'pendaftar_mln.id')
            ->orderBy('pembayaran_mln.id', 'desc')
            ->select('pembayaran_mln.*', 'pembayaran_mln.id as id', 'pendaftar_mln.*')
            ->get();

        return $pembayaran;
    }
}