<?php

namespace App\Modules\TKManagement\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RegisterGelombang extends Model {
	protected $table = 'gelombang';

    public function __construct() {
        $this->table = 'gelombang';
    }

    public function getGelombangByKelas($kelas){
    	$gelombang = DB::table('gelombang')
    					->where('kelas',$kelas)
    					->first();
    	return $gelombang;
    }

    public function getGelombangByDate($datenow){
        $gelombang = DB::table('gelombang')
                        ->where('periode', $datenow)
                        ->first();
        return $gelombang;
    }
    
}