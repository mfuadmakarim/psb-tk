<?php
//SMS Gateway
Route::group(['namespace' => 'App\Modules\CommonModule\Controllers', 'middleware' => ['web'], 'prefix' => '/api/sms'], function() {
    //create message
    Route::GET('new/single', 'SmsGatewayController@sendRegisterSuccessConfirmation');
});