<?php
Route::group(['namespace' => 'App\Modules\Auth\Controllers', 'middleware' => 'web', 'prefix' => '/auth'], function() {

    //Home
    Route::get('/login', 'LoginController@index');
    Route::post('/check', 'LoginController@authenticate');
   
 });